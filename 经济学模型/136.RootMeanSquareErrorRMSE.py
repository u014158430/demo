# 

# 

# 均方根误差（Root Mean Square Error，简称RMSE）是一种常用的评估预测模型性能的指标。它衡量了预测值与实际值之间的差异程度，通过计算预测值与实际值之间差值的平方和的平均值，再取平方根得到。RMSE的值越小，表示模型的预测能力越好，预测值与实际值之间的偏差越小。

# 

# ### 原理解释

# 

# RMSE的计算公式如下：

# 

# \[ RMSE = \sqrt{\frac{1}{n} \sum_{i=1}^{n} (y_i - \hat{y}_i)^2 } \]

# 

# 其中，\( y_i \) 是第 \( i \) 个样本的实际值，\( \hat{y}_i \) 是模型对第 \( i \) 个样本的预测值，\( n \) 是样本数量。

# 

# RMSE的计算过程可以分为以下几个步骤：

# 

# 1. **计算差值**：对于每个样本，计算其预测值与实际值之间的差值，即 \( y_i - \hat{y}_i \)。

# 2. **计算差值的平方**：将每个差值平方，以消除正负号的影响，并强调较大的误差。

# 3. **计算平均值**：将所有差值的平方相加，然后除以样本数量 \( n \)，得到差值的平方的平均值。

# 4. **取平方根**：最后，对差值的平方的平均值取平方根，得到RMSE。

# 

# RMSE的优点在于它对较大的误差非常敏感，因此能够很好地反映模型的预测能力。然而，由于它涉及到平方和开方运算，因此计算起来可能相对复杂一些。

# 

# ### Python代码示例

# 

# 下面是一个使用Python计算RMSE的示例代码：

# 

# 

import numpy as np



# 假设我们有以下实际值和预测值

y_true = np.array([3.0, -0.5, 2.0, 7.0])  # 实际值

y_pred = np.array([2.5, 0.0, 2.1, 7.8])   # 预测值



# 计算RMSE

def rmse(y_true, y_pred):

    """

    计算均方根误差（RMSE）

    

    参数:

    y_true (np.array): 实际值数组

    y_pred (np.array): 预测值数组

    

    返回:

    float: RMSE值

    """

    # 确保两个数组的长度相同

    assert len(y_true) == len(y_pred), "实际值和预测值的数量必须相等"

    

    # 计算差值的平方

    squared_errors = (y_true - y_pred) ** 2

    

    # 计算差值的平方的平均值

    mean_squared_error = np.mean(squared_errors)

    

    # 取平方根得到RMSE

    rmse_value = np.sqrt(mean_squared_error)

    

    return rmse_value



# 调用函数计算RMSE

rmse_result = rmse(y_true, y_pred)

print(f"RMSE: {rmse_result}")
