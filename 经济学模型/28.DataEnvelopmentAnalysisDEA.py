# 

# 

# **数据包络分析（Data Envelopment Analysis, DEA）详解与Python代码示例**

# 

# **一、数据包络分析（DEA）详解**

# 

# 数据包络分析（DEA）是一种非参数的评价方法，主要用于评估具有多个投入和多个产出的决策单元（Decision Making Units, DMUs）的相对效率。DEA方法最初由A.Charnes和W.W.Cooper在1978年提出，现已广泛应用于生产管理、金融分析、教育评估等多个领域。

# 

# DEA的核心思想是通过比较不同DMUs的输入和输出数据，确定一个效率前沿（Efficiency Frontier），即所有DMUs中效率最高的集合。位于效率前沿上的DMUs被认为是相对有效的，而位于效率前沿下方的DMUs则被认为是相对无效的。DEA方法可以帮助管理者识别出效率低下的DMUs，并为其改进提供方向。

# 

# DEA模型通常分为两类：CRS（Constant Returns to Scale）模型和VRS（Variable Returns to Scale）模型。CRS模型假设DMUs的规模效率是恒定的，即输入和输出之间存在线性关系；而VRS模型则假设DMUs的规模效率是可变的，即输入和输出之间存在非线性关系。在实际应用中，可以根据具体情况选择合适的模型。

# 

# **二、Python代码示例**

# 

# 以下是一个使用Python实现DEA模型的简单示例。这里我们采用CRS模型（也称为CCR模型），并使用Gurobi作为优化求解器。请注意，为了运行此代码，您需要安装Gurobi和相应的Python库。

# 

# 

import gurobipy as gp

import pandas as pd



# 假设我们有三个DMUs（A, B, C），每个DMUs有两个输入（x1, x2）和两个输出（y1, y2）

inputs = pd.DataFrame({

    'DMU': ['A', 'B', 'C'],

    'x1': [2, 1, 3],

    'x2': [3, 2, 4]

})

outputs = pd.DataFrame({

    'DMU': ['A', 'B', 'C'],

    'y1': [4, 3, 6],

    'y2': [5, 4, 7]

})



# 合并输入和输出数据

data = pd.merge(inputs, outputs, on='DMU')



# 设置Gurobi模型

m = gp.Model('DEA_CCR')



# 定义变量

lambdas = m.addVars(data.shape[0], name='lambda')

OE = m.addVar(name='OE', lb=0, ub=1, vtype=gp.GRB.CONTINUOUS)

s_neg = m.addVars(data.shape[1]-2, name='s_neg')  # 输入的松弛变量

s_pos = m.addVars(2, name='s_pos')  # 输出的松弛变量



# 设置目标函数（最大化OE）

m.setObjective(OE, gp.GRB.MAXIMIZE)



# 设置约束条件

for i in range(data.shape[0]):

    if i == 0:  # 对于第一个DMU，设置OE为其效率值

        m.addConstr(OE * data.loc[i, 'x1':] <= gp.quicksum(lambdas[j] * data.loc[j, 'x1':] for j in range(data.shape[0])))

        m.addConstr(data.loc[i, 'y1':] >= gp.quicksum(lambdas[j] * data.loc[j, 'y1':] for j in range(data.shape[0])) - s_pos)

    else:  # 对于其他DMU，设置其权重为非负

        m.addConstr(lambdas[i] >= 0)



# 设置松弛变量为非负

for var in s_neg:

    m.addConstr(var >= 0)

for var in s_pos:

    m.addConstr(var >= 0)



# 求解模型

m.optimize()



# 输出结果

for v in m.getVars():

    print(v.varName, v.x)
