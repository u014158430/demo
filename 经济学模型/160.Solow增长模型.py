# 

# 

# **Solow增长模型详解与Python代码示例**

# 

# **一、Solow增长模型概述**

# 

# Solow增长模型，又称新古典经济增长模型或外生经济增长模型，是由经济学家罗伯特·索洛（Robert Solow）提出的一个重要的经济增长理论模型。该模型在新古典经济学的框架下，探讨了经济增长的动力和机制。Solow增长模型的核心在于其假设条件和生产函数，通过这些设定，模型能够分析储蓄率、人口增长率、技术进步等因素对经济增长的影响。

# 

# **二、Solow增长模型假设**

# 

# 1. 储蓄全部转化为投资，即储蓄-投资转化率假设为1。

# 2. 投资的边际收益率递减，即投资的规模收益是常数。

# 3. 采用资本和劳动可替代的新古典科布-道格拉斯生产函数。

# 

# **三、Solow增长模型生产函数**

# 

# 新古典主义生产函数的形式是 Y = A * F(K, L)，其中Y代表总产出，A代表全要素生产率（技术进步），K代表资本总量，L代表劳动力总量。该函数满足规模报酬不变和边际产出递减的假设。

# 

# **四、Solow增长模型基本方程**

# 

# 在Solow增长模型中，基本方程描述了人均资本存量k的变化率k'。方程为：

# k' = s * Af(k) + (1 - δ)k - nk

# 

# 其中，s为储蓄率，A为技术水平，f(k)为人均生产函数，δ为资本折旧率，n为人口增长率。方程左侧表示人均资本存量的变化率，右侧第一项表示储蓄转化为投资后增加的人均资本存量，第二项表示折旧和人口增长导致的人均资本存量减少。

# 

# **五、Python代码示例**

# 

# 下面是一个简单的Python代码示例，用于模拟Solow增长模型的基本方程。

# 

# 

import math

import numpy as np



# 参数设定

s = 0.25  # 储蓄率

A = 1.0   # 技术水平

delta = 0.05  # 资本折旧率

n = 0.02  # 人口增长率

alpha = 0.3  # 资本产出弹性（科布-道格拉斯生产函数中的参数）



# 科布-道格拉斯生产函数

def F(K, L, A=A, alpha=alpha):

    return A * (K ** alpha) * (L ** (1 - alpha))



# 人均生产函数

def f(k, A=A, alpha=alpha):

    return F(k, 1, A, alpha)



# Solow增长模型基本方程

def solow_growth_model(k, s=s, A=A, delta=delta, n=n, alpha=alpha):

    dkdt = s * f(k, A, alpha) + (1 - delta) * k - n * k

    return dkdt



# 初始人均资本存量

k_initial = 1.0



# 模拟时间步长和总时间

dt = 0.01

T = 100



# 模拟Solow增长模型

k_path = [k_initial]

for t in np.arange(0, T, dt):

    dkdt = solow_growth_model(k_path[-1])

    k_next = k_path[-1] + dkdt * dt

    k_path.append(k_next)



# 输出结果

print("人均资本存量随时间变化的路径：")

for t, k in enumerate(k_path):

    print(f"t={t*dt}, k={k}")
