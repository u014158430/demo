# 

# 

# **Newsvendor模型详解与Python代码示例**

# 

# 一、Newsvendor模型概述

# 

# Newsvendor模型，也称为报童模型，是库存管理中的一个经典模型，用于解决在需求不确定的情况下，如何确定最优的库存水平以最大化期望利润。该模型假设零售商每天从供应商处采购一定数量的商品（如报纸），然后以固定价格销售给顾客。如果商品售罄，则失去销售更多商品的机会；如果商品过剩，则面临库存积压和可能的清仓成本。

# 

# 在Newsvendor模型中，零售商需要权衡缺货成本和过剩成本，以确定最佳的采购量。其中，缺货成本包括因缺货导致的销售额损失和可能的顾客流失成本；过剩成本则包括库存积压成本、存储成本和可能的清仓成本。

# 

# 二、Newsvendor模型参数与假设

# 

# 1. 单位采购成本：c，即每采购一个商品所需的成本。

# 2. 零售价：p，即每个商品的售价。

# 3. 残值：s，即商品未售出时的回收价值或清仓价格。

# 4. 需求：D，为一个随机变量，表示每天的商品需求量，其概率分布函数和密度分布函数分别为F(d)和f(d)。

# 5. 假设p > c > s，即售价高于采购成本，且采购成本高于残值。

# 

# 三、Newsvendor模型目标

# 

# Newsvendor模型的目标是找到最优的采购量Q，使得期望利润最大化。期望利润E[π(Q)]的计算公式如下：

# 

# E[π(Q)] = p * ∫_0^Q x * f(x) dx + s * ∫_Q^∞ (Q - x) * f(x) dx - c * Q

# 

# 其中，第一项表示当需求量小于等于采购量时，销售额与采购成本之差；第二项表示当需求量大于采购量时，残值与采购成本之差；第三项为采购成本。

# 

# 四、Python代码示例

# 

# 下面是一个使用Python实现Newsvendor模型的简单示例。假设需求量D服从均值为μ、标准差为σ的正态分布：

# 

# 

import numpy as np

from scipy.stats import norm

import matplotlib.pyplot as plt



# 参数设定

c = 10  # 单位采购成本

p = 20  # 零售价

s = 5   # 残值

mu = 100  # 需求量均值

sigma = 20  # 需求量标准差



# 定义期望利润函数

def expected_profit(Q):

    # 计算正态分布的概率密度函数

    x = np.linspace(0, mu + 3*sigma, 1000)

    f_x = norm.pdf(x, mu, sigma)

    

    # 计算期望利润

    profit_when_demand_less_than_Q = p * np.sum(x[x <= Q] * f_x[x <= Q])

    profit_when_demand_greater_than_Q = s * np.sum((Q - x[x > Q]) * f_x[x > Q])

    total_cost = c * Q

    expected_profit_value = profit_when_demand_less_than_Q + profit_when_demand_greater_than_Q - total_cost

    return expected_profit_value



# 求解最优采购量

Q_values = np.linspace(0, mu + 3*sigma, 100)

expected_profits = [expected_profit(Q) for Q in Q_values]

optimal_Q = Q_values[np.argmax(expected_profits)]



print(f"最优采购量为：{optimal_Q}")

print(f"最大期望利润为：{np.max(expected_profits)}")



# 可视化期望利润与采购量的关系

plt.plot(Q_values, expected_profits)

plt.xlabel('采购量Q')

plt.ylabel('期望利润')

plt.title('Newsvendor Model: 期望利润与采购量的关系')

plt.show()
