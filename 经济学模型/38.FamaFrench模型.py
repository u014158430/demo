# 

# 

# Fama-French模型，特别是三因子模型（Fama-French Three-Factor Model），是金融领域用于解释股票收益率的一个重要模型。该模型由Eugene Fama和Kenneth French在1993年提出，旨在改进传统的资本资产定价模型（CAPM），以更好地解释股票市场的实际表现。

# 

# ### 模型解释

# 

# Fama-French三因子模型认为，一个投资组合（或单只股票）的超额回报率（即实际收益率与无风险收益率之差）可以由三个因子来解释：市场风险因子、市值因子和账面市值比因子。

# 

# 1. **市场风险因子**：代表了市场整体的风险水平，通常用市场投资组合的收益率与无风险利率之差来衡量。

# 2. **市值因子**：反映了公司规模对股票收益率的影响。通常，小市值公司的股票收益率会高于大市值公司。

# 3. **账面市值比因子**：即公司的账面价值与市场价值之比，反映了公司的价值被市场低估或高估的程度。高账面市值比的公司（即市场价值低于账面价值的公司）通常被认为具有更高的投资价值。

# 

# 模型的具体形式为：

# 

# \[ R_{it} - R_{ft} = \alpha_i + \beta_1 (R_{mt} - R_{ft}) + \beta_2 SMB_t + \beta_3 HML_t + \epsilon_{it} \]

# 

# 其中，\( R_{it} \) 是资产i在时间t的收益率，\( R_{ft} \) 是无风险收益率，\( R_{mt} \) 是市场投资组合在时间t的收益率，\( SMB_t \) 是市值因子，\( HML_t \) 是账面市值比因子，\( \beta_1, \beta_2, \beta_3 \) 是各因子的系数，\( \alpha_i \) 是截距项，\( \epsilon_{it} \) 是残差项。

# 

# ### Python代码示例

# 

# 以下是一个使用Python和`statsmodels`库实现Fama-French三因子模型的简单示例。请注意，为了简化示例，这里假设你已经有了市场风险因子、市值因子和账面市值比因子的数据。

# 

# 

import pandas as pd

import statsmodels.api as sm



# 假设df是一个包含以下列的DataFrame：

# 'Return': 股票收益率

# 'MarketRisk': 市场风险因子（R_mt - R_ft）

# 'SMB': 市值因子

# 'HML': 账面市值比因子



# 添加常数项作为截距项

df['const'] = 1



# 定义因变量和自变量

X = df[['const', 'MarketRisk', 'SMB', 'HML']]

y = df['Return']



# 使用OLS（最小二乘法）进行线性回归

model = sm.OLS(y, X)

results = model.fit()



# 输出回归结果

print(results.summary())



# 提取alpha值（截距项）和beta值（系数）

alpha = results.params['const']

beta_market = results.params['MarketRisk']

beta_smb = results.params['SMB']

beta_hml = results.params['HML']



print(f"Alpha: {alpha}")

print(f"Beta Market: {beta_market}")

print(f"Beta SMB: {beta_smb}")

print(f"Beta HML: {beta_hml}")
