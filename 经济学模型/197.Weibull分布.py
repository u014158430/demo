# 

# 

# **Weibull分布详解与Python代码示例**

# 

# Weibull分布，也被称为韦伯分布、韦氏分布或威布尔分布，是一种在可靠性分析、寿命数据建模以及许多其他领域中广泛应用的连续概率分布。该分布由瑞典工程师和数学家Wallodi Weibull在1951年提出，并以其名字命名。Weibull分布因其能够灵活地描述各种形状的数据分布，特别是具有偏斜和长尾特性的数据，而备受青睐。

# 

# **一、Weibull分布的特性**

# 

# Weibull分布的概率密度函数（PDF）由两个参数决定：尺度参数λ（lambda）和形状参数k（kappa）。这两个参数决定了分布的形状和尺度。当k>1时，分布是右偏的，具有长尾部；当k<1时，分布是左偏的，具有短尾部；而当k=1时，Weibull分布退化为指数分布。

# 

# **二、Python代码示例**

# 

# 下面是一个使用Python的SciPy库来计算和绘制Weibull分布概率密度函数（PDF）和累积分布函数（CDF）的示例代码。

# 

# 

# 导入必要的库

import numpy as np

import matplotlib.pyplot as plt

from scipy.stats import weibull_min



# 设置Weibull分布的参数

shape_param = 2.0  # 形状参数k

scale_param = 3.0  # 尺度参数λ



# 生成Weibull分布样本数据（可选，用于后续分析或可视化）

# sample_data = weibull_min.rvs(shape_param, scale=scale_param, size=1000)



# 创建一个x值的范围，用于计算和绘制PDF和CDF

x = np.linspace(weibull_min.ppf(0.01, shape_param, scale=scale_param),

                weibull_min.ppf(0.99, shape_param, scale=scale_param), 100)



# 计算PDF和CDF

pdf = weibull_min.pdf(x, shape_param, scale=scale_param)

cdf = weibull_min.cdf(x, shape_param, scale=scale_param)



# 绘制PDF和CDF图像

plt.figure(figsize=(10, 4))



# 绘制PDF图像

plt.subplot(1, 2, 1)

plt.plot(x, pdf, 'r-', lw=2, label='PDF')

plt.xlabel('x')

plt.ylabel('Probability Density')

plt.title('Weibull Distribution PDF')

plt.legend()



# 绘制CDF图像

plt.subplot(1, 2, 2)

plt.plot(x, cdf, 'b-', lw=2, label='CDF')

plt.xlabel('x')

plt.ylabel('Cumulative Probability')

plt.title('Weibull Distribution CDF')

plt.legend()



# 调整子图布局并显示图像

plt.tight_layout()

plt.show()
