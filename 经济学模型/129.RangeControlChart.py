# 

# 

# **Range Control Chart（范围控制图）详解与Python代码示例**

# 

# **一、范围控制图（Range Control Chart）概述**

# 

# 范围控制图（Range Control Chart），也称为R控制图，是一种用于监控和评估生产过程中子组内部变异性的统计工具。在质量控制领域，R控制图通常与Xbar控制图（均值图）一起使用，以提供关于生产过程变异性的额外信息。R控制图的主要目的是通过监测子组范围（即子组内最大值与最小值之差）的变化，来识别生产过程中可能存在的异常或变异。

# 

# **二、范围控制图的绘制原理**

# 

# 1. **数据收集**：与Xbar控制图类似，R控制图的数据通常来源于生产过程中的子组样本。每个子组包含一定数量的连续生产的产品或测量值。

# 2. **计算范围**：对于每个子组，计算其最大值与最小值之差，得到该子组的范围（R）。

# 3. **计算控制限**：R控制图的控制限通常基于子组范围的平均值（平均范围）加减一个常数倍数的标准差。这个常数通常是3，意味着控制限覆盖了大约99.7%的数据点，假设数据遵循正态分布。

# 4. **绘制图表**：使用图表工具（如Python的matplotlib库）绘制R控制图，包括垂直轴（表示范围的数值）、水平轴（表示子组的编号或生产时间的顺序）、中心线（表示所有子组范围的平均值）、上控制限和下控制限。

# 

# **三、Python代码示例**

# 

# 下面是一个使用Python绘制R控制图的简单示例：

# 

# 

import numpy as np

import matplotlib.pyplot as plt



# 假设我们有以下子组范围数据

subgroup_ranges = [5.2, 4.8, 5.1, 5.3, 4.9, 5.0, 4.7, 5.4, 5.1, 4.6]



# 计算平均范围和控制限

mean_range = np.mean(subgroup_ranges)

std_range = np.std(subgroup_ranges, ddof=1)  # ddof=1表示计算样本标准差

UCL = mean_range + 3 * std_range

LCL = mean_range - 3 * std_range



# 绘制R控制图

plt.figure(figsize=(10, 6))

plt.plot(subgroup_ranges, marker='o', linestyle='-', color='blue', label='Subgroup Ranges')

plt.axhline(y=mean_range, color='red', linestyle='--', label='Center Line')

plt.axhline(y=UCL, color='green', linestyle='--', label='Upper Control Limit')

plt.axhline(y=LCL, color='green', linestyle='--', label='Lower Control Limit')



plt.title('Range Control Chart')

plt.xlabel('Subgroup Number')

plt.ylabel('Range')

plt.legend()

plt.grid(True)

plt.show()
