# 

# 

# **过程能力指数（Process Capability Index）详解与Python代码示例**

# 

# 过程能力指数（Process Capability Index，简称PCI或Cpk）是六西格玛管理中的一个核心概念，用于量化评估一个生产过程满足产品质量规格要求的能力。它基于统计学的原理，通过计算过程的平均值、标准差以及规格限（上下限），来评估过程的稳定性和一致性。

# 

# ### 过程能力指数详解

# 

# 过程能力指数主要关注两个方面的能力：一是过程的固有波动（由标准差σ表示），二是过程均值与规格中心之间的偏移（由偏移度K表示）。当过程的波动较小且均值与规格中心重合时，过程能力指数较高，表示过程具有较高的稳定性和一致性，能够满足产品质量规格的要求。

# 

# 过程能力指数的计算通常涉及以下几个步骤：

# 

# 1. **收集数据**：首先，需要收集一组来自生产过程的测量数据。

# 2. **计算过程参数**：包括过程的平均值（μ）、标准差（σ）以及规格限（USL和LSL）。

# 3. **计算偏移度K**：偏移度K表示过程均值与规格中心之间的偏移程度，计算公式为K = |M-μ| / (T/2)，其中M为规格中心，T为规格范围（USL-LSL）。

# 4. **计算过程能力指数**：常用的过程能力指数有Cp和Cpk。Cp仅考虑过程的波动，不考虑偏移；而Cpk则同时考虑波动和偏移。Cpk的计算公式为Cpk = (1 - 2|M-μ|/T) * T/6σ = T/6σ - |M-μ|/3σ。

# 

# ### Python代码示例

# 

# 下面是一个使用Python计算过程能力指数的示例代码：

# 

# 

import numpy as np



# 假设我们有一组测量数据

data = [10, 12, 11, 13, 9, 10, 12, 13, 11, 10]



# 计算平均值和标准差

mean = np.mean(data)

std_dev = np.std(data, ddof=1)  # ddof=1表示使用n-1作为分母的无偏估计



# 设定规格限

lower_limit = 8

upper_limit = 15



# 计算规格范围和规格中心

spec_range = upper_limit - lower_limit

spec_center = (upper_limit + lower_limit) / 2



# 计算偏移度K

K = abs(spec_center - mean) / (spec_range / 2)



# 计算过程能力指数Cpk

Cpk = (1 - 2 * K) * (spec_range / (6 * std_dev))



# 输出结果

print(f"平均值（μ）: {mean}")

print(f"标准差（σ）: {std_dev}")

print(f"偏移度（K）: {K}")

print(f"过程能力指数（Cpk）: {Cpk}")



# 根据Cpk的值判断过程的稳定性

if Cpk > 1.33:

    print("过程稳定")

elif Cpk > 1:

    print("过程可行")

else:

    print("过程不可行，需要改进")
