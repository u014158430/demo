# 

# 

# Markowitz投资组合理论，也称为现代投资组合理论（Modern Portfolio Theory, MPT），由经济学家Harry Markowitz在1952年提出，是现代金融学的基石之一。该理论的核心思想是通过分散化投资来降低非系统性风险，同时追求在给定的风险水平下最大化预期收益，或在给定的预期收益下最小化风险。

# 

# ### 理论解释

# 

# Markowitz投资组合理论基于两个关键概念：预期收益率（均值）和风险（方差）。它将投资组合的预期收益率视为组成该组合的各资产预期收益率的加权平均，而将投资组合的风险视为各资产收益率的协方差矩阵的函数。通过优化这两个参数，投资者可以找到所谓的“有效边界”（Efficient Frontier），即给定风险水平下预期收益率最高的投资组合集合。

# 

# ### Python代码示例

# 

# 下面是一个使用Python和`numpy`、`scipy`库实现Markowitz投资组合理论的简单示例。我们将使用随机生成的资产收益率数据来演示如何找到有效边界。

# 

# 

import numpy as np

from scipy.optimize import minimize



# 假设我们有5只股票，其历史收益率数据如下（这里使用随机数据代替）

np.random.seed(0)  # 设置随机种子以便结果可复现

returns = np.random.normal(size=(100, 5))  # 假设有100个历史数据点



# 计算预期收益率和协方差矩阵

expected_returns = returns.mean(axis=0)

cov_matrix = np.cov(returns, rowvar=False)



# 定义目标函数（负夏普比率，因为我们想最小化它）

def negative_sharpe_ratio(weights, expected_returns, cov_matrix, risk_free_rate=0.01):

    portfolio_return = np.dot(weights, expected_returns)

    portfolio_volatility = np.sqrt(np.dot(weights.T, np.dot(cov_matrix, weights)))

    sharpe_ratio = (portfolio_return - risk_free_rate) / portfolio_volatility

    return -sharpe_ratio  # 返回负值以便使用minimize函数



# 定义约束条件（权重和为1，且权重非负）

constraints = ({'type': 'eq', 'fun': lambda weights: np.sum(weights) - 1},

               {'type': 'ineq', 'fun': lambda weights: weights})



# 初始权重（等权重分配）

init_guess = np.array([1/len(expected_returns)] * len(expected_returns))



# 使用scipy的minimize函数找到最优权重

bounds = tuple((0, 1) for asset in range(len(expected_returns)))

result = minimize(negative_sharpe_ratio, init_guess,

                  args=(expected_returns, cov_matrix),

                  method='SLSQP',

                  bounds=bounds,

                  constraints=constraints)



# 输出结果

optimal_weights = result.x

print(f"Optimal weights: {optimal_weights}")

print(f"Expected return: {np.dot(optimal_weights, expected_returns)}")

print(f"Portfolio volatility: {np.sqrt(np.dot(optimal_weights.T, np.dot(cov_matrix, optimal_weights)))}")
