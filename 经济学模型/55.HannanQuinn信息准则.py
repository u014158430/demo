# 

# 

# **Hannan-Quinn信息准则（HQIC）详解与Python代码示例**

# 

# 在统计学和机器学习中，模型选择是一个至关重要的步骤，它决定了我们如何从一组候选模型中选择出最佳模型。Hannan-Quinn信息准则（HQIC）是众多模型选择准则之一，特别适用于时间序列分析等场景。HQIC由Hannan和Quinn在1979年提出，旨在解决自回归（AR）时间序列模型中的定阶问题。

# 

# ### HQIC准则详解

# 

# HQIC准则基于信息论的原理，通过比较不同模型的拟合优度和复杂度来评估模型的性能。具体来说，HQIC由两部分组成：一部分是模型拟合数据的残差平方和（衡量模型对数据的拟合程度），另一部分是对模型复杂度的惩罚项（即模型中参数的个数）。HQIC的计算公式如下：

# 

# \[ \text{HQIC} = \log(\sigma^2) + 2k\log(\log(T)) \]

# 

# 其中，\(\sigma^2\) 是模型的残差平方和，\(k\) 是模型中参数的个数（包括截距项和滞后项系数），\(T\) 是样本容量。HQIC值越小，表示模型对数据的拟合越好，且复杂度适中。

# 

# ### Python代码示例

# 

# 下面是一个使用Python计算HQIC的示例代码。假设我们有一个简单的自回归模型（AR模型），并希望从几个不同阶数的模型中选择最佳模型。

# 

# 

import numpy as np

from statsmodels.tsa.ar_model import AutoReg

from scipy.stats import norm



# 模拟数据生成

np.random.seed(0)  # 设置随机种子以便结果可复现

n_samples = 100  # 样本数量

ar_params = [1.5, -0.7]  # 真实AR模型参数

errors = norm.rvs(size=n_samples)  # 生成误差项

data = np.zeros(n_samples)

for i in range(1, n_samples):

    data[i] = ar_params[0] * data[i-1] + ar_params[1] * data[i-2] + errors[i]



# 计算HQIC

def calculate_hqic(model, y):

    residuals = y - model.predict(start=model.k_ar, end=len(y)-1)

    sigma2 = np.sum(residuals**2) / (len(y) - model.k_ar - 1)

    k = model.k_ar + 1  # 参数个数（包括截距项）

    T = len(y)

    hqic = np.log(sigma2) + 2 * k * np.log(np.log(T))

    return hqic



# 拟合不同阶数的AR模型并计算HQIC

best_hqic = float('inf')

best_model = None

for p in range(1, 5):  # 尝试1到4阶的AR模型

    model = AutoReg(data, lags=p).fit()

    hqic = calculate_hqic(model, data)

    if hqic < best_hqic:

        best_hqic = hqic

        best_model = model



# 输出最佳模型的阶数和HQIC值

print(f"Best model order: {best_model.k_ar}")

print(f"HQIC value: {best_hqic}")
