# 

# 

# **贝尔曼-福特算法（Bellman-Ford Algorithm）详解与Python代码示例**

# 

# **一、算法详解**

# 

# 贝尔曼-福特算法是一种用于求解带权图中单源最短路径问题的算法。与迪杰斯特拉（Dijkstra）算法不同，贝尔曼-福特算法可以处理包含负权边的图，并且能够检测图中是否存在负权环（即权值之和小于0的环路）。

# 

# 算法的核心思想是通过不断的“松弛”操作来逐步逼近最短路径。具体来说，对于图中的每一条边，如果从一个节点到另一个节点的直接路径比当前已知的最短路径更短，那么就更新这个最短路径。这个过程会重复进行，直到没有更多的路径可以被更新为止。

# 

# 贝尔曼-福特算法的基本步骤如下：

# 

# 1. **初始化**：将所有节点到源节点的距离初始化为无穷大（表示当前还不知道最短路径），源节点到自身的距离为0。

# 2. **松弛操作**：对于图中的每一条边，如果从一个节点到另一个节点的直接路径比当前已知的最短路径更短，那么就更新这个最短路径。这个过程会重复进行V-1次（V是图中节点的数量），以确保所有可能的最短路径都被考虑到。

# 3. **检测负权环**：在完成V-1次松弛操作后，再对图中的每一条边进行一次额外的松弛操作。如果在这个过程中还能找到更短的路径，那么就说明图中存在负权环。

# 

# **二、Python代码示例**

# 

# 下面是一个使用Python实现的贝尔曼-福特算法的示例代码：

# 

# 

# 定义一个图类

class Graph:

    def __init__(self, vertices):

        self.V = vertices  # 节点数量

        self.graph = []  # 存储边的列表



    # 添加边的方法

    def add_edge(self, u, v, w):

        self.graph.append([u, v, w])



    # 贝尔曼-福特算法的实现

    def bellman_ford(self, src):

        # 初始化距离数组和前驱节点数组

        dist = [float('inf')] * self.V

        dist[src] = 0

        pred = [-1] * self.V



        # V-1次松弛操作

        for _ in range(self.V - 1):

            for u, v, w in self.graph:

                if dist[u] != float('inf') and dist[u] + w < dist[v]:

                    dist[v] = dist[u] + w

                    pred[v] = u



        # 检测负权环

        for u, v, w in self.graph:

            if dist[u] != float('inf') and dist[u] + w < dist[v]:

                print("图中存在负权环")

                return



        # 打印结果

        print("源节点到各个节点的最短距离：")

        for i in range(self.V):

            print(f"到节点{i}的最短距离：{dist[i]}")



# 创建一个图对象并添加边

g = Graph(5)

g.add_edge(0, 1, -1)

g.add_edge(0, 2, 4)

g.add_edge(1, 2, 3)

g.add_edge(1, 3, 2)

g.add_edge(1, 4, 2)

g.add_edge(3, 2, 5)

g.add_edge(3, 1, 1)

g.add_edge(4, 3, -3)



# 执行贝尔曼-福特算法

g.bellman_ford(0)
