# 

# 

# Spline Interpolation（样条插值）是一种数学方法，用于在给定的数据点之间构造一条平滑的曲线。在数据分析和图形绘制中，样条插值特别有用，因为它能够生成通过所有给定数据点的连续曲线，同时保持曲线的平滑性。三次样条插值（Cubic Spline Interpolation）是样条插值的一种，它在每个区间上都是一个三次多项式，并且在整个插值区间上保持一阶和二阶导数的连续性。

# 

# ### 原理解释

# 

# 三次样条插值的基本思想是通过给定的数据点（x_i, y_i），其中i=0, 1, ..., n，构造一个分段定义的三次多项式函数S(x)，使得S(x)在每个子区间[x_i, x_{i+1}]上都是一个三次多项式，并且满足以下条件：

# 

# 1. S(x_i) = y_i，即函数值在数据点上等于给定的值。

# 2. S(x)在[a, b]区间内是连续的，其中a=x_0, b=x_n。

# 3. S(x)的一阶导数（斜率）和二阶导数（曲率）在[a, b]区间内也是连续的。

# 

# 为了求解这个三次样条插值函数，我们需要确定每个子区间上的三次多项式的系数。这通常通过解一个线性方程组来实现，该方程组由样条曲线的连续性条件和边界条件组成。

# 

# ### Python代码示例

# 

# 下面是一个使用Python实现三次样条插值的示例代码：

# 

# 

import numpy as np

from scipy.interpolate import CubicSpline

import matplotlib.pyplot as plt



# 给定的数据点

x = np.array([0, 1, 2, 3, 4])

y = np.array([0, 0.8, 0.9, 0.1, -0.8])



# 创建CubicSpline插值对象

spline = CubicSpline(x, y)



# 生成插值点进行绘图

x_fine = np.linspace(x[0], x[-1], 1000)

y_fine = spline(x_fine)



# 绘制原始数据点和插值曲线

plt.plot(x, y, 'o', label='Data points')

plt.plot(x_fine, y_fine, label='Cubic Spline')



# 添加图例和标题

plt.legend()

plt.title('Cubic Spline Interpolation')

plt.xlabel('x')

plt.ylabel('y')



# 显示图形

plt.grid(True)

plt.show()
