# 

# 

# **均匀分布（Uniform Distribution）详解与Python代码示例**

# 

# 在统计学和概率论中，均匀分布是一种非常基础且重要的概率分布。它描述了一个随机变量在某一区间内取任意值的可能性是相等的。均匀分布分为离散型均匀分布和连续型均匀分布两种。离散型均匀分布指的是在有限个数值上，每个数值出现的概率都是相等的；而连续型均匀分布则指的是在一个连续区间内，随机变量取该区间内任意值的概率密度是相等的。

# 

# 下面，我们将以连续型均匀分布为例，给出详细的解释和Python代码示例。

# 

# **一、连续型均匀分布**

# 

# 连续型均匀分布的概率密度函数（PDF）可以表示为：

# 

# f(x) = 1 / (b - a)，当 a ≤ x ≤ b

# f(x) = 0，当 x < a 或 x > b

# 

# 其中，a 和 b 是区间的两个端点，且 a < b。从概率密度函数可以看出，在区间 [a, b] 内，随机变量取任意值的概率密度都是相等的，即 1 / (b - a)。

# 

# **二、统计特征**

# 

# 连续型均匀分布的统计特征包括均值、方差、偏度和峰度等。对于标准的连续均匀分布（a = 0, b = 1），其均值为 0.5，方差为 1/12，偏度为 0，峰度为 -1.2。

# 

# **三、Python代码示例**

# 

# 下面是一个使用Python和NumPy库生成连续型均匀分布随机变量并计算其统计特征的示例代码：

# 

# 

import numpy as np

import matplotlib.pyplot as plt



# 设置均匀分布的区间

a = 0

b = 1



# 生成连续型均匀分布的随机变量

# 使用NumPy的random.uniform函数，生成指定区间内的随机浮点数

samples = np.random.uniform(a, b, size=10000)



# 计算统计特征

mean = np.mean(samples)

variance = np.var(samples)

skewness = scipy.stats.skew(samples)  # 需要导入scipy.stats模块

kurtosis = scipy.stats.kurtosis(samples)  # 需要导入scipy.stats模块



# 打印统计特征

print(f"均值: {mean}")

print(f"方差: {variance}")

print(f"偏度: {skewness}")

print(f"峰度: {kurtosis}")



# 绘制概率密度函数图

# 使用matplotlib库绘制直方图来近似表示概率密度函数

plt.hist(samples, bins=50, density=True, alpha=0.6, color='g')

plt.title('连续型均匀分布的概率密度函数')

plt.xlabel('随机变量值')

plt.ylabel('概率密度')

plt.grid(True)

plt.show()
