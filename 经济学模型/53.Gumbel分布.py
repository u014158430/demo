# 

# 

# **Gumbel分布详解与Python代码示例**

# 

# **一、Gumbel分布概述**

# 

# Gumbel分布，也称为耿贝尔分布或极值I型分布，是一种连续概率分布，常用于描述极值事件的分布情况。在统计学和概率论中，Gumbel分布常用于建模那些具有极值特性的随机变量，例如自然灾害（如洪水、地震）的严重程度、金融市场的极端波动等。

# 

# Gumbel分布有两个参数：位置参数μ和尺度参数β。位置参数μ决定了分布的中心位置，而尺度参数β则决定了分布的离散程度。当μ=0且β=1时，Gumbel分布被称为标准Gumbel分布。

# 

# **二、Gumbel分布的概率密度函数和分布函数**

# 

# Gumbel分布的概率密度函数（PDF）为：

# 

# f(x; μ, β) = (1/β) * exp(-(x-μ)/β) * exp(-exp(-(x-μ)/β))

# 

# 其中，x为随机变量，μ为位置参数，β为尺度参数。

# 

# Gumbel分布的分布函数（CDF）为：

# 

# F(x; μ, β) = exp(-exp(-(x-μ)/β))

# 

# **三、Python代码示例**

# 

# 下面是一个使用Python和SciPy库实现Gumbel分布拟合的示例代码：

# 

# 

# 导入必要的库

import numpy as np

import matplotlib.pyplot as plt

from scipy.stats import gumbel_r



# 生成一些符合Gumbel分布的随机数据

np.random.seed(0)  # 设置随机数种子以便结果可复现

mu = 0  # 位置参数

beta = 1  # 尺度参数

data = gumbel_r.rvs(size=1000, loc=mu, scale=beta)



# 使用SciPy的gumbel_r类进行参数估计

loc, scale = gumbel_r.fit(data)



# 绘制原始数据和拟合的Gumbel分布曲线

x = np.linspace(min(data) - 3*scale, max(data) + 3*scale, 100)

y = gumbel_r.pdf(x, loc, scale)



plt.hist(data, bins=30, density=True, alpha=0.6, color='g', label='Data')

plt.plot(x, y, 'r-', lw=2, label='Fitted Gumbel')



# 设置图例和标题

plt.legend(loc='best')

plt.title('Gumbel Distribution Fitting')

plt.xlabel('Value')

plt.ylabel('Probability Density')



# 显示图形

plt.show()



# 输出估计的参数值

print(f"Estimated location parameter: {loc}")

print(f"Estimated scale parameter: {scale}")
