# 

# 

# 概率密度函数（Probability Density Function，简称PDF）是概率论中用于描述连续型随机变量概率分布的重要工具。它表示了随机变量在某个特定取值点附近的概率分布情况，而不是直接给出该点上的概率值（因为对于连续型随机变量，取到某个特定值的概率严格来说为0）。概率密度函数通常用f(x)表示，其中x是随机变量的取值。

# 

# 概率密度函数具有两个基本性质：非负性和规范性。非负性意味着f(x)的值总是大于等于0，因为概率不能为负；规范性则要求f(x)在整个定义域上的积分等于1，这保证了随机变量取到所有可能值的概率之和为1。

# 

# 在Python中，我们可以使用SciPy库中的stats模块来计算和绘制概率密度函数。以下是一个使用SciPy库和Matplotlib库绘制标准正态分布概率密度函数的示例代码：

# 

# 

# 导入必要的库

import numpy as np

import matplotlib.pyplot as plt

from scipy.stats import norm



# 生成一组服从标准正态分布的随机数（用于绘制直方图对比）

data = np.random.randn(1000)



# 创建一个x值的数组，用于计算概率密度函数

x = np.linspace(-4, 4, 1000)  # 从-4到4，共1000个点



# 使用norm.pdf函数计算x值对应的概率密度函数值

pdf_values = norm.pdf(x)



# 绘制概率密度函数图像

plt.plot(x, pdf_values, 'r-', lw=2, label='Probability Density Function')



# 绘制随机数的直方图，并设置density=True以使其与概率密度函数进行比较

plt.hist(data, bins=30, density=True, alpha=0.5, label='Histogram of Data')



# 设置图例

plt.legend(loc='upper left')



# 设置x轴和y轴的标签

plt.xlabel('Value')

plt.ylabel('Probability Density')



# 设置标题

plt.title('Probability Density Function of Standard Normal Distribution')



# 显示图像

plt.show()
