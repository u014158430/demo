# 

# 

# Kanban系统，也被称为看板系统，是一种源自丰田生产方式的项目管理和生产控制工具。其核心思想是通过可视化手段来管理任务和工作流程，以实现高效、灵活和持续改进的生产或项目执行。Kanban系统通常将任务或工作项展示在“看板”上，这些看板可以是物理的（如白板、卡片等），也可以是电子的（如软件应用、网页等）。

# 

# 在Kanban系统中，任务通常被分配到不同的列中，以表示它们当前的状态，如“待办”、“进行中”和“已完成”等。团队成员可以实时查看任务的状态和进度，并根据需要进行任务的分配、优先级调整或进度更新。

# 

# 下面是一个简单的Python代码示例，用于模拟一个基本的Kanban系统。这个示例使用了Python的字典和列表数据结构来存储和管理任务信息。

# 

# 

# 定义一个Kanban类，用于管理看板系统

class Kanban:

    def __init__(self):

        # 初始化看板上的列

        self.columns = {

            '待办': [],

            '进行中': [],

            '已完成': [],

        }



    # 添加任务到待办列

    def add_task_to_todo(self, task_name):

        self.columns['待办'].append(task_name)



    # 将任务从待办列移动到进行中列

    def move_task_to_in_progress(self, task_name):

        if task_name in self.columns['待办']:

            self.columns['待办'].remove(task_name)

            self.columns['进行中'].append(task_name)

        else:

            print(f"任务 {task_name} 不在待办列中")



    # 将任务从进行中列移动到已完成列

    def move_task_to_done(self, task_name):

        if task_name in self.columns['进行中']:

            self.columns['进行中'].remove(task_name)

            self.columns['已完成'].append(task_name)

        else:

            print(f"任务 {task_name} 不在进行中列中")



    # 显示看板上的所有任务

    def show_board(self):

        print("看板:")

        for column_name, tasks in self.columns.items():

            print(f"{column_name}: {tasks}")



# 使用示例

if __name__ == "__main__":

    # 创建一个Kanban实例

    kanban = Kanban()



    # 添加任务到待办列

    kanban.add_task_to_todo("编写代码")

    kanban.add_task_to_todo("测试代码")

    kanban.add_task_to_todo("编写文档")



    # 显示看板上的任务

    kanban.show_board()



    # 将任务从待办列移动到进行中列

    kanban.move_task_to_in_progress("编写代码")



    # 显示更新后的看板

    kanban.show_board()



    # 将任务从进行中列移动到已完成列

    kanban.move_task_to_done("编写代码")



    # 显示最终状态的看板

    kanban.show_board()
