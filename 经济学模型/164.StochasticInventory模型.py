# 

# 

# **随机存贮模型（Stochastic Inventory Model）详解与Python代码示例**

# 

# 随机存贮模型是一种用于解决库存管理中不确定性问题的数学模型。在供应链管理中，由于市场需求、供应时间、运输成本等多种因素的不确定性，库存管理变得尤为复杂。随机存贮模型通过引入概率论和随机过程的理论，为库存管理提供了一种有效的决策工具。

# 

# ### 随机存贮模型概述

# 

# 随机存贮模型主要关注在随机需求下，如何确定最优的订货量、订货时间以及库存水平，以最小化库存成本（包括存贮成本和缺货成本）。这类模型通常假设需求是随机离散的，即每个周期内的需求量是一个随机变量，其概率分布是已知的。

# 

# 随机存贮模型有多种类型，如报童问题（Newsboy Problem）、(s, S)型存贮模型等。其中，报童问题是最简单的单品种单周期随机存贮模型，它假设每天报纸的需求量是一个随机变量，报童需要决定每天订购多少份报纸以最大化利润。

# 

# ### Python代码示例

# 

# 下面是一个简单的Python代码示例，用于模拟报童问题的决策过程。假设我们已知每天报纸需求量的概率分布，并假设每份报纸的进价为c，售价为p，未售出的报纸每份的残值为s。我们的目标是找到最优的订货量Q，以最大化期望利润。

# 

# 

import numpy as np



# 假设报纸需求量的概率分布

demand_prob = {0: 0.1, 1: 0.2, 2: 0.3, 3: 0.2, 4: 0.1, 5: 0.1}  # 字典形式表示概率分布



# 报纸的进价、售价和残值

c = 1  # 进价

p = 2  # 售价

s = 0.5  # 残值



# 计算期望利润的函数

def expected_profit(Q):

    profit = 0

    for r in demand_prob:

        # 如果需求量小于等于订货量，则售出r份报纸，剩余Q-r份报纸退回并获得残值

        # 如果需求量大于订货量，则售出Q份报纸，损失r-Q份报纸的潜在利润

        if r <= Q:

            profit += demand_prob[r] * (r * p + (Q - r) * s - Q * c)

        else:

            profit += demand_prob[r] * (Q * p + (r - Q) * 0 - Q * c)

    return profit



# 遍历可能的订货量，找到使期望利润最大的订货量

optimal_Q = 0

max_profit = float('-inf')

for Q in range(max(demand_prob.keys()) + 1):  # 遍历所有可能的需求量

    profit = expected_profit(Q)

    if profit > max_profit:

        max_profit = profit

        optimal_Q = Q



print(f"最优订货量为：{optimal_Q}")

print(f"期望最大利润为：{max_profit}")
