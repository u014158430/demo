# 

# 

# Harris-Todaro模型，也称为托达罗模型（Todaro Model），是由经济学家Harris和Todaro共同提出的，旨在解释发展中国家在二元经济结构下劳动力从农村向城市迁移的动因和后果。该模型特别关注城市失业问题对劳动力迁移决策的影响，与刘易斯模型（Lewis Model）的观点形成鲜明对比。

# 

# ### Harris-Todaro模型解释

# 

# Harris-Todaro模型假设在一个由农业部门和工业部门（或城市部门）组成的二元经济结构中，劳动力迁移的决策不仅基于城乡之间的实际工资差异，还受到城市预期收入的影响。这里的预期收入是指考虑到城市失业率后的预期工资水平。即使城市存在失业，只要城市部门的预期收入高于农村部门的实际收入，农村劳动力仍会向城市迁移。

# 

# 模型的核心思想是，由于城市部门工资具有某种程度的刚性（如最低工资标准），即使城市存在失业，城市部门的工资水平也不会迅速下降。因此，即使城市失业率上升，只要城市部门的预期收入仍高于农村，劳动力迁移仍会发生。这种迁移可能导致城市失业问题进一步加剧，因为新迁移的劳动力增加了城市劳动力的供给。

# 

# ### Python代码示例

# 

# 虽然Harris-Todaro模型本身并不直接涉及编程或数据分析，但我们可以使用Python来模拟该模型的一些基本假设和结果。以下是一个简单的Python代码示例，用于模拟劳动力迁移的决策过程。

# 

# 

import numpy as np



# 假设参数

rural_wage = 100  # 农村工资水平

urban_wage = 200  # 城市工资水平（不考虑失业）

urban_unemployment_rate = 0.2  # 城市失业率

migration_threshold = 120  # 迁移的期望工资阈值（农村工资的某个倍数）



# 计算城市部门的预期收入

def calculate_expected_urban_income(urban_wage, urban_unemployment_rate):

    expected_income = urban_wage * (1 - urban_unemployment_rate)

    return expected_income



# 模拟劳动力迁移决策

def simulate_migration_decision(rural_wage, expected_urban_income, migration_threshold):

    if expected_urban_income > migration_threshold:

        return "Migrate to urban area"

    else:

        return "Stay in rural area"



# 计算城市部门的预期收入

expected_urban_income = calculate_expected_urban_income(urban_wage, urban_unemployment_rate)



# 模拟劳动力迁移决策

migration_decision = simulate_migration_decision(rural_wage, expected_urban_income, migration_threshold)



print(f"Rural wage: {rural_wage}")

print(f"Urban wage (without considering unemployment): {urban_wage}")

print(f"Urban unemployment rate: {urban_unemployment_rate * 100}%")

print(f"Expected urban income: {expected_urban_income}")

print(f"Migration decision: {migration_decision}")
