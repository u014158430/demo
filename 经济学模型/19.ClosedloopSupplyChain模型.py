# 

# 

# **Closed-loop Supply Chain模型详解与Python代码示例**

# 

# **一、Closed-loop Supply Chain模型概述**

# 

# 闭环供应链（Closed-Loop Supply Chain, CLSC）是一种新型的供应链管理策略，它涵盖了从原材料采购、产品制造、分销、消费到产品回收、再利用或处置的整个生命周期。与传统的线性供应链相比，闭环供应链更注重资源的循环利用和环境的可持续发展。通过有效的闭环供应链管理，企业可以降低生产成本、提高资源利用效率、减少环境污染，并实现经济效益和环境效益的双赢。

# 

# 在闭环供应链模型中，主要参与者包括原材料供应商、制造商、分销商、消费者以及回收商等。这些参与者通过信息流、物流和资金流的紧密协作，形成了一个闭合的循环体系。在这个体系中，产品经过使用后被回收，经过检测、分类、再处理等环节，重新进入生产环节或作为再生资源被利用，从而实现了资源的循环利用和环境的可持续发展。

# 

# **二、Python代码示例**

# 

# 以下是一个简化的闭环供应链模型Python代码示例，用于模拟产品的生产、销售和回收过程。

# 

# 

# 导入所需库

import numpy as np



# 定义产品类

class Product:

    def __init__(self, id, quantity):

        self.id = id

        self.quantity = quantity

        self.status = 'new'  # 产品状态：new（新品）、sold（已售）、recycled（已回收）



    def sell(self):

        if self.status == 'new':

            self.status = 'sold'

            print(f"产品 {self.id} 已售出，剩余数量：{self.quantity - 1}")

            self.quantity -= 1

        else:

            print("产品已售或已回收，无法再次销售。")



    def recycle(self):

        if self.status == 'sold':

            self.status = 'recycled'

            print(f"产品 {self.id} 已回收，等待处理。")

        else:

            print("产品非已售状态，无法回收。")



# 模拟生产、销售和回收过程

def simulate_clsc(production_quantity, sales_quantity, recycle_rate):

    # 初始化产品列表

    products = [Product(i, production_quantity) for i in range(1, 101)]  # 假设生产100个产品



    # 模拟销售过程

    for product in products:

        if product.quantity > 0:

            product.sell()

            if product.quantity <= 0:

                break  # 假设只销售sales_quantity个产品



    # 模拟回收过程

    recycled_count = 0

    for product in products:

        if product.status == 'sold' and np.random.rand() < recycle_rate:  # 按回收率随机回收产品

            product.recycle()

            recycled_count += 1



    print(f"共回收了 {recycled_count} 个产品。")



# 调用函数进行模拟

simulate_clsc(production_quantity=100, sales_quantity=80, recycle_rate=0.6)
