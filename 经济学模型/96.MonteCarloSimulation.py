# 

# 

# 蒙特卡洛模拟（Monte Carlo Simulation）是一种基于随机数的计算方法，它通过大量随机抽样来模拟复杂系统的行为，并据此进行预测和分析。这种方法得名于摩纳哥的蒙特卡洛赌场，因为其中涉及的随机性和不确定性与赌博游戏相似。蒙特卡洛模拟在金融、工程、物理、生物、环境、医学等多个领域都有广泛的应用。

# 

# ### 基本原理

# 

# 蒙特卡洛模拟的核心思想是通过随机抽样来近似计算一个复杂问题的解。首先，我们需要明确需要解决的问题，并确定问题中涉及的所有变量和参数。然后，为这些变量生成随机数，这些随机数遵循变量的概率分布。接下来，使用这些随机数构建问题的实例或场景，并在这些场景上执行所需的计算或分析。最后，重复上述步骤多次，每次使用不同的随机数，并对多次模拟的结果进行统计分析，从而得到问题的统计特性，如期望值、方差、置信区间等。

# 

# ### Python代码示例

# 

# 下面是一个使用Python进行蒙特卡洛模拟计算π值的示例代码：

# 

# 

import numpy as np

import matplotlib.pyplot as plt



# 设置模拟次数

n_simulations = 1000000



# 初始化变量

n_points_circle = 0

n_points_total = 0



# 进行模拟

for _ in range(n_simulations):

    # 在[-1, 1]范围内随机生成x和y坐标

    x = np.random.uniform(-1, 1)

    y = np.random.uniform(-1, 1)

    

    # 判断点是否在单位圆内（即x^2 + y^2 <= 1）

    if x**2 + y**2 <= 1:

        n_points_circle += 1

    

    # 无论点是否在圆内，都增加总点数

    n_points_total += 1



# 计算π的近似值（使用蒙特卡洛方法）

# π的近似值 = (落在圆内的点数 / 总点数) * 4（因为我们在2x2的正方形内模拟）

pi_estimate = (n_points_circle / n_points_total) * 4



# 打印结果

print(f"经过{n_simulations}次模拟，π的近似值为：{pi_estimate}")



# 可视化结果（可选）

plt.scatter(np.random.uniform(-1, 1, n_points_total), np.random.uniform(-1, 1, n_points_total), s=1, color='gray')

circle = plt.Circle((0, 0), 1, fill=False)

plt.gca().add_artist(circle)

plt.xlim(-1.5, 1.5)

plt.ylim(-1.5, 1.5)

plt.title('Monte Carlo Simulation for Estimating Pi')

plt.show()
