# 

# 

# ### Barabási–Albert模型详解与Python代码示例

# 

# #### 模型介绍

# 

# Barabási–Albert（BA）模型是一种用于模拟和分析复杂网络结构的数学模型，特别适用于描述那些具有“无标度”特性的网络。无标度网络是指网络中节点的连接度（度）分布遵循幂律分布，即少数节点拥有大量的连接，而大多数节点只有少量的连接。这种特性在许多现实世界的网络中都得到了体现，如社交网络、互联网、生物网络等。

# 

# BA模型基于两个核心机制：增长和优先连接。增长意味着网络中的节点数量会随着时间的推移而增加；优先连接则是指新加入的节点更倾向于与那些已经拥有较多连接的节点建立连接，即“富者愈富”的原则。这种机制导致了网络的无标度特性，即少数节点（称为“集线器”或“中心节点”）拥有大量的连接，而大多数节点则只有少量的连接。

# 

# #### Python代码示例

# 

# 下面是一个使用Python实现的简单BA模型代码示例。该代码模拟了网络的增长过程，并展示了如何根据优先连接原则建立新的连接。

# 

# 

import random

from collections import defaultdict



# 初始化网络，使用defaultdict来存储节点的邻居列表

G = defaultdict(list)



# 初始节点数

initial_nodes = 10

# 初始时，随机添加initial_nodes个节点

for i in range(initial_nodes):

    G[i] = []



# 新增节点数

new_nodes = 100

# 模拟网络增长过程

for i in range(initial_nodes, initial_nodes + new_nodes):

    # 选择m个已存在的节点进行连接

    m = 2  # 每个新节点连接的边数

    connected_nodes = random.sample(G.keys(), k=m)

    

    # 根据优先连接原则，选择连接的概率与节点的度成正比

    for node in connected_nodes:

        # 使用节点的度作为权重进行概率选择是不准确的，因为需要归一化

        # 这里为了简化，我们假设所有已存在节点的度之和为total_degree（实际中需要计算）

        total_degree = sum(len(neighbors) for neighbors in G.values())

        # 计算选择当前节点的概率（这里简化为度/总度）

        probability = len(G[node]) / total_degree

        

        # 根据概率选择是否真正连接（这里简化为直接连接）

        G[i].append(node)

        G[node].append(i)  # 无向图，所以两边都要添加



# 打印网络的部分属性

print("网络中的节点数:", len(G))

print("节点的度分布示例:")

for node, neighbors in G.items():

    print(f"节点{node}的度: {len(neighbors)}")



# 注意：上述代码为了简化，没有精确实现优先连接的概率选择，而是直接连接了m个节点。

# 在实际实现中，需要计算每个节点的选择概率，并根据概率进行随机选择。
