# 

# 

# **Single-Server Queue（单服务器队列）详解与Python代码示例**

# 

# **一、概念解释**

# 

# Single-Server Queue（单服务器队列）是一种排队模型，其中只有一个服务节点（或称为服务器）来处理到达队列的请求或任务。这种模型在现实生活中有广泛的应用，比如银行柜台、超市收银台、客服中心等。在这些场景中，客户或请求按照某种规则（如先到先服务FIFO、优先级等）排队等待服务，而服务器则按照队列中的顺序或优先级来提供服务。

# 

# 在Single-Server Queue模型中，通常需要考虑以下几个关键因素：

# 

# 1. **到达过程**：请求或任务如何到达队列，是随机到达还是按照某种规律到达。

# 2. **服务过程**：服务器如何处理队列中的请求或任务，每个请求或任务的服务时间是多少。

# 3. **队列规则**：队列中请求或任务的排序规则，如FIFO、LIFO、优先级等。

# 

# **二、Python代码示例**

# 

# 下面是一个使用Python的`queue`模块实现的Single-Server Queue的简单示例。在这个示例中，我们模拟了一个银行柜台的场景，客户按照先到先服务的规则排队等待服务。

# 

# 

import queue

import threading

import time

import random



# 定义一个单服务器队列类

class SingleServerQueue:

    def __init__(self):

        self.queue = queue.Queue()  # 使用Python内置的队列模块

        self.server_thread = None   # 服务器线程



    def start_server(self):

        # 启动服务器线程

        self.server_thread = threading.Thread(target=self._server_loop)

        self.server_thread.start()



    def _server_loop(self):

        # 服务器循环处理队列中的请求

        while True:

            if not self.queue.empty():

                customer = self.queue.get()  # 从队列中取出一个客户

                print(f"开始服务客户：{customer}")

                time.sleep(random.randint(1, 3))  # 模拟服务时间

                print(f"服务完成客户：{customer}")

                self.queue.task_done()  # 标记任务完成



    def add_customer(self, customer):

        # 客户到达并加入队列

        self.queue.put(customer)

        print(f"客户{customer}加入队列，等待服务...")



# 使用示例

if __name__ == "__main__":

    ssq = SingleServerQueue()

    ssq.start_server()  # 启动服务器线程



    # 模拟客户到达并加入队列

    for i in range(10):

        customer_id = i + 1

        time.sleep(random.randint(0, 2))  # 模拟客户随机到达时间

        ssq.add_customer(customer_id)



    # 等待所有客户服务完成

    ssq.queue.join()

    print("所有客户均已服务完成，系统关闭。")
