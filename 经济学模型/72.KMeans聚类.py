# 

# 

# K-Means聚类是一种广泛使用的无监督学习算法，用于将数据点划分为K个集群（或称为簇）。该算法基于迭代优化的思想，通过计算数据点与聚类中心（质心）的距离，将数据点分配给最近的聚类中心，然后重新计算聚类中心的位置，直到达到收敛条件。

# 

# ### 原理解释

# 

# K-Means聚类的核心思想是将相似的数据点聚集在一起，形成不同的簇。算法首先随机选择K个数据点作为初始的聚类中心（质心），然后计算每个数据点与这K个质心的距离，将数据点分配给距离最近的质心所在的簇。完成所有数据点的分配后，算法会重新计算每个簇的质心，即该簇所有数据点的平均值。这个过程会不断重复，直到质心的位置不再发生显著变化，或者达到预设的最大迭代次数。

# 

# ### Python代码示例

# 

# 下面是一个使用Python和sklearn库实现K-Means聚类的示例代码：

# 

# 

# 导入必要的库

from sklearn.cluster import KMeans

from sklearn.datasets import make_blobs

import matplotlib.pyplot as plt



# 生成模拟数据集

# make_blobs函数用于生成随机的数据点，这里生成了300个数据点，分为4个簇

X, y = make_blobs(n_samples=300, centers=4, cluster_std=0.60, random_state=0)



# 绘制原始数据点

plt.scatter(X[:, 0], X[:, 1], s=50)

plt.title('Original Data')

plt.show()



# 初始化KMeans对象，设置聚类数为4

kmeans = KMeans(n_clusters=4, random_state=0)



# 对数据进行聚类

y_kmeans = kmeans.fit_predict(X)



# 绘制聚类结果

plt.scatter(X[y_kmeans == 0, 0], X[y_kmeans == 0, 1], s=50, c='red', label='Cluster 1')

plt.scatter(X[y_kmeans == 1, 0], X[y_kmeans == 1, 1], s=50, c='blue', label='Cluster 2')

plt.scatter(X[y_kmeans == 2, 0], X[y_kmeans == 2, 1], s=50, c='green', label='Cluster 3')

plt.scatter(X[y_kmeans == 3, 0], X[y_kmeans == 3, 1], s=50, c='yellow', label='Cluster 4')



# 绘制聚类中心（质心）

plt.scatter(kmeans.cluster_centers_[:, 0], kmeans.cluster_centers_[:, 1], s=250, marker='*', c='black', label='Centroids')



plt.title('K-Means Clustering')

plt.legend()

plt.show()



# 输出聚类中心和每个簇的样本数

print("Cluster centers:")

print(kmeans.cluster_centers_)

print("\nLabels of each sample:")

print(y_kmeans)
