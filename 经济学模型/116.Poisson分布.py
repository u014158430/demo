# 

# 

# **Poisson分布详解与Python代码示例**

# 

# Poisson分布，又称为泊松分布，是统计学和概率论中一种重要的离散概率分布。它主要用于描述在固定时间或空间内，某一事件发生的次数。例如，电话交换机在一段时间内接到的呼叫次数、放射性物质在单位时间内发射出的粒子数等，都可以使用Poisson分布进行建模。

# 

# Poisson分布的概率质量函数（Probability Mass Function, PMF）定义为：

# 

# \[ P(X=k) = \frac{e^{-\lambda} \lambda^k}{k!} \quad (k=0, 1, 2, \ldots) \]

# 

# 其中，\( X \) 是随机变量，表示事件发生的次数；\( k \) 是具体的次数；\( \lambda \) 是分布的参数，表示单位时间（或空间）内事件发生的平均次数。

# 

# 下面，我们将通过Python代码来展示如何计算Poisson分布的概率，并绘制其概率质量函数图。

# 

# 

# 导入必要的库

import numpy as np

import matplotlib.pyplot as plt

from scipy.stats import poisson



# 设置λ值

lambda_val = 5  # 假设单位时间内事件发生的平均次数为5



# 计算0到15次事件发生的概率

k_values = np.arange(0, 16)  # 次数范围从0到15

pmf_values = poisson.pmf(k_values, lambda_val)  # 使用scipy的poisson.pmf函数计算概率



# 绘制概率质量函数图

plt.figure(figsize=(10, 6))

plt.bar(k_values, pmf_values, color='blue', alpha=0.6, label='Poisson PMF')

plt.xlabel('Number of Events (k)')

plt.ylabel('Probability')

plt.title('Poisson Distribution with λ = {}'.format(lambda_val))

plt.legend()

plt.grid(True)

plt.show()



# 示例：计算特定次数事件发生的概率

# 例如，计算发生3次事件的概率

probability_of_3_events = poisson.pmf(3, lambda_val)

print(f"The probability of 3 events occurring is: {probability_of_3_events:.4f}")



# 示例：计算发生不超过5次事件的累积概率

# 使用scipy的poisson.cdf函数计算累积概率

cumulative_probability_up_to_5 = poisson.cdf(5, lambda_val)

print(f"The cumulative probability of 5 or fewer events occurring is: {cumulative_probability_up_to_5:.4f}")
