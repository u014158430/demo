# 

# 

# **随机游走（Random Walk）理论详解与Python代码示例**

# 

# 随机游走理论，作为一种数学统计模型，描述了由一系列随机步骤组成的路径。这种路径的每一步都是随机的，没有明确的方向，因此整体呈现出一种不规则的变动形式。随机游走理论在自然科学、社会科学以及工程领域都有广泛的应用，如花粉在水滴表面的运动、分子的扩散过程、股票价格的波动等。

# 

# 在随机游走模型中，每一步的移动方向和距离都是随机选择的。例如，在二维平面上，每一步可以向上、下、左、右四个方向移动，而移动的距离可以是固定的，也可以是随机生成的。通过不断重复这个过程，就可以生成一条随机游走的路径。

# 

# 下面是一个使用Python实现随机游走模型的示例代码：

# 

# 

import random

import matplotlib.pyplot as plt



# 定义一个RandomWalk类，用于模拟随机游走过程

class RandomWalk:

    

    # 初始化方法，设置随机游走的步数

    def __init__(self, num_points=5000):

        self.num_points = num_points

        # 初始位置设为原点

        self.x_values = [0]

        self.y_values = [0]



    # 填充随机游走路径的方法

    def fill_walk(self):

        # 循环生成指定步数的随机游走路径

        while len(self.x_values) < self.num_points:

            # 随机选择x轴和y轴上的移动方向（1表示正方向，-1表示负方向）

            x_direction = random.choice([1, -1])

            y_direction = random.choice([1, -1])

            

            # 随机选择移动的距离（这里简化为1，也可以设置为其他值或随机值）

            step = 1

            

            # 计算下一个点的位置

            next_x = self.x_values[-1] + x_direction * step

            next_y = self.y_values[-1] + y_direction * step

            

            # 将下一个点添加到路径中

            self.x_values.append(next_x)

            self.y_values.append(next_y)



# 创建一个RandomWalk对象，并填充随机游走路径

rw = RandomWalk()

rw.fill_walk()



# 绘制随机游走路径

plt.style.use('classic')

fig, ax = plt.subplots()

ax.scatter(rw.x_values, rw.y_values, s=15)  # 绘制路径上的点，s参数控制点的大小

plt.show()  # 显示图形
