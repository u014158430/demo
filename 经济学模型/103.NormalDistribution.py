# 

# 

# 正态分布（Normal Distribution），又称高斯分布（Gaussian Distribution），是一种在自然界和许多科学领域中广泛存在的概率分布。它的特点是数据集中在一个均值附近，并且数据分布的形状呈钟形，即中间高、两头低，且左右对称。正态分布由两个参数决定：均值（μ）和标准差（σ），它们分别决定了分布的中心位置和分布的幅度。

# 

# 在Python中，我们可以使用NumPy库来生成正态分布的随机数，并使用Matplotlib库来绘制正态分布的概率密度函数（PDF）和直方图。下面是一个详细的解释和代码示例：

# 

# 

# 导入必要的库

import numpy as np

import matplotlib.pyplot as plt

from scipy.stats import norm  # 导入scipy库中的norm模块，用于计算正态分布的概率密度函数



# 设置正态分布的均值和标准差

mu = 0  # 均值

sigma = 1  # 标准差



# 使用NumPy的random.normal函数生成正态分布的随机数

# 这里我们生成1000个随机数

samples = np.random.normal(mu, sigma, 1000)



# 绘制直方图

count, bins, ignored = plt.hist(samples, 30, density=True, alpha=0.6, color='g')

plt.xlabel('Value')

plt.ylabel('Frequency')

plt.title('Histogram of Normal Distribution')



# 绘制正态分布的概率密度函数

# 使用scipy.stats.norm.pdf函数计算每个bin中心对应的概率密度值

pdf_values = norm.pdf(bins, mu, sigma)

plt.plot(bins, pdf_values, 'r', linewidth=2)



# 添加图例

plt.legend(['Normal Distribution (μ=0, σ=1)', 'Histogram'], loc='upper left')



# 显示图形

plt.grid(True)

plt.show()
