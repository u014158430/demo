# 

# 

# Petri网（Petri Net）是一种用于描述并发、异步、分布式系统的数学和图形建模工具。它由德国数学家Carl Adam Petri在1962年提出，并在系统科学、计算机科学、自动化控制等领域得到了广泛应用。

# 

# ### Petri网的基本概念

# 

# Petri网由两种节点（库所和变迁）以及有向弧组成。库所（Place）用于描述系统的状态，可以包含一定数量的令牌（Token），代表系统资源的数量或系统的某种状态。变迁（Transition）则代表系统中可能发生的活动或事件，变迁的触发（Fire）会导致令牌在库所之间的移动，从而改变系统的状态。

# 

# ### Petri网的图形表示

# 

# Petri网通常使用图形来表示，其中库所用圆圈表示，变迁用矩形或粗线框表示，有向弧用箭头表示。令牌用小黑点表示，并放在库所中。

# 

# ### Python代码示例

# 

# 下面是一个简单的Petri网模型的Python代码示例，用于模拟一个简单的生产-消费系统。在这个系统中，生产者（Producer）将产品放入仓库（Warehouse），消费者（Consumer）从仓库中取出产品。

# 

# 

class PetriNet:

    def __init__(self):

        # 库所：Producer, Warehouse, Consumer

        self.places = {

            'Producer': 0,  # 生产者库所，初始时没有令牌

            'Warehouse': 0,  # 仓库库所，初始时没有令牌

            'Consumer': 0   # 消费者库所，初始时没有令牌（但在此模型中不直接使用）

        }

        

        # 变迁：Produce, Consume

        self.transitions = {

            'Produce': False,  # 生产变迁，初始时未触发

            'Consume': False   # 消费变迁，初始时未触发

        }



    def fire_transition(self, transition_name):

        """触发变迁"""

        if transition_name == 'Produce' and self.places['Producer'] == 0:

            # 如果生产者库所中有令牌，则触发Produce变迁

            self.places['Producer'] -= 1  # 消耗生产者库所中的令牌（此处仅为示意）

            self.places['Warehouse'] += 1  # 在仓库库所中添加令牌

            self.transitions['Produce'] = False  # 变迁已触发，重置为False

            print("Produced an item. Warehouse now has {} items.".format(self.places['Warehouse']))

        elif transition_name == 'Consume' and self.places['Warehouse'] > 0:

            # 如果仓库库所中有令牌，则触发Consume变迁

            self.places['Warehouse'] -= 1  # 消耗仓库库所中的令牌

            self.places['Consumer'] += 1  # 在消费者库所中添加令牌（此处仅为示意）

            self.transitions['Consume'] = False  # 变迁已触发，重置为False

            print("Consumed an item. Warehouse now has {} items.".format(self.places['Warehouse']))

        else:

            print("Cannot fire transition {}.".format(transition_name))



    def add_token_to_producer(self):

        """向生产者库所中添加令牌（模拟生产者生产产品）"""

        self.places['Producer'] += 1

        print("Added a token to Producer. Now has {} tokens.".format(self.places['Producer']))



# 示例用法

petri_net = PetriNet()

petri_net.add_token_to_producer()  # 生产者生产了一个产品

petri_net.fire_transition('Produce')  # 触发Produce变迁，将产品放入仓库

petri_net.fire_transition('Consume')  # 触发Consume变迁，消费者从仓库中取出一个产品
