# 

# 

# ### 爬山算法（Hill Climbing Algorithm）详解与Python代码示例

# 

# #### 一、算法详解

# 

# 爬山算法（Hill Climbing Algorithm）是一种基于贪心策略的启发式搜索算法，主要用于解决优化问题。其核心思想是从一个初始解开始，通过不断选择邻域中使目标函数值更优的解，逐步逼近局部最优解。爬山算法简单直观，但存在陷入局部最优解的风险，即可能错过全局最优解。

# 

# #### 算法步骤

# 

# 1. **初始化**：选择一个初始解作为起点。

# 2. **评估当前解**：计算当前解的目标函数值。

# 3. **生成邻域解**：在当前解的邻域内生成一个或多个候选解。

# 4. **评估邻域解**：计算每个候选解的目标函数值。

# 5. **选择最优邻域解**：从候选解中选择目标函数值最优的解作为新的当前解。

# 6. **迭代**：重复步骤2至5，直到满足停止条件（如达到最大迭代次数或无法找到更优解）。

# 

# #### 二、Python代码示例

# 

# 下面是一个使用爬山算法求解一元函数最大值的Python代码示例。假设我们要找到函数`f(x) = -(x - 3)^2 + 10`的最大值。

# 

# 

import random



# 定义目标函数

def objective_function(x):

    return -(x - 3) ** 2 + 10



# 爬山算法实现

def hill_climbing(x0, step_size, max_iterations):

    current_x = x0  # 初始解

    current_value = objective_function(current_x)  # 初始解的目标函数值

    

    for iteration in range(max_iterations):

        # 生成邻域解

        neighbors = [current_x + step_size, current_x - step_size]

        

        # 评估邻域解并选择最优解

        best_neighbor = max(neighbors, key=objective_function)

        best_neighbor_value = objective_function(best_neighbor)

        

        # 如果找到了更优的解，则更新当前解

        if best_neighbor_value > current_value:

            current_x = best_neighbor

            current_value = best_neighbor_value

        else:

            # 如果没有找到更优的解，则停止迭代

            break

    

    # 返回最优解和对应的目标函数值

    return current_x, current_value



# 设置初始解、步长和最大迭代次数

initial_solution = random.uniform(-10, 10)

step_size = 0.1

max_iterations = 100



# 执行爬山算法

optimal_solution, optimal_value = hill_climbing(initial_solution, step_size, max_iterations)



# 输出结果

print(f"Optimal solution: x = {optimal_solution:.4f}")

print(f"Optimal value: f(x) = {optimal_value:.4f}")
