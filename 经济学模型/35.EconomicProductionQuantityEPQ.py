# 

# 

# 经济生产批量（Economic Production Quantity，EPQ）是生产管理中一个重要的概念，它指的是在一定时间内，为达到生产成本和库存成本之和最低而确定的生产数量。EPQ模型考虑了生产准备成本、单位产品的生产成本以及库存持有成本，通过优化这些成本来确定最佳的生产批量。

# 

# ### 解释

# 

# EPQ模型主要适用于单一制造过程的生产模式，其中生产批量的大小直接影响到生产成本和库存成本。生产批量越大，更换生产线的次数就越少，从而降低了生产准备成本，但同时也会增加库存持有成本。因此，EPQ模型的目标就是找到一个平衡点，使得总成本最低。

# 

# 在EPQ模型中，通常假设以下条件成立：

# 

# 1. 对库存系统的需求率是常量。

# 2. 一次生产批量无最大最小限制。

# 3. 生产准备费用是固定的，与生产批量无关。

# 4. 单位产品的生产成本是固定的。

# 5. 库存持有成本是库存量的线性函数。

# 6. 不允许缺货。

# 7. 需要连续补充库存。

# 

# ### Python代码示例

# 

# 下面是一个使用Python计算EPQ的示例代码，并附有详细的注释：

# 

# 

import math



# 定义EPQ计算函数

def calculate_epq(demand_per_year, setup_cost, holding_cost_per_unit_per_year):

    """

    计算经济生产批量（EPQ）。

    

    参数:

        demand_per_year (float): 年需求量。

        setup_cost (float): 生产准备成本。

        holding_cost_per_unit_per_year (float): 单位产品每年的库存持有成本。

        

    返回:

        float: 经济生产批量（EPQ）。

    """

    # 使用EPQ公式进行计算

    epq = math.sqrt((2 * demand_per_year * setup_cost) / holding_cost_per_unit_per_year)

    return epq



# 示例参数

demand_per_year = 10000  # 年需求量

setup_cost = 500  # 生产准备成本

holding_cost_per_unit_per_year = 10  # 单位产品每年的库存持有成本



# 计算EPQ

epq_value = calculate_epq(demand_per_year, setup_cost, holding_cost_per_unit_per_year)



# 输出结果

print(f"经济生产批量（EPQ）为：{epq_value:.2f}件")

print(f"这意味着，为了最小化生产成本和库存成本之和，企业每年应该按照{epq_value:.2f}件的批量进行生产。")



# 额外计算（可选）

# 例如，计算每年应生产的次数和每年的总成本（这里仅作为示例，具体计算可能需要根据实际情况进行调整）

production_runs_per_year = demand_per_year / epq_value

total_annual_cost = (

    setup_cost * production_runs_per_year  # 生产准备成本

    + (holding_cost_per_unit_per_year / 2) * epq_value  # 库存持有成本（假设库存量在一年中均匀分布）

    # 这里没有考虑单位产品的生产成本，因为它在EPQ模型中通常被视为常量，不影响最佳生产批量的确定

)



print(f"每年应生产的次数为：{production_runs_per_year:.2f}次")

print(f"每年的总成本（仅包括生产准备成本和库存持有成本）为：{total_annual_cost:.2f}元")
