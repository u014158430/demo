# 

# 

# Hotelling模型，也称为霍特林模型，是经济学中用于分析空间竞争和选址问题的重要理论模型。该模型由经济学家Harold Hotelling在1929年提出，主要用于解释在具有空间差异的市场中，企业如何决定其产品的价格和位置。在Hotelling模型中，产品在物质性能上被认为是相同的，但消费者由于地理位置的差异而面临不同的运输成本，因此他们更关心的是价格与运输成本之和。

# 

# ### Hotelling模型的基本假设

# 

# 1. **线性城市**：假设存在一个长度为1的线性城市，消费者均匀地分布在这个区间内。

# 2. **两个商店**：城市中有两个商店，分别位于城市的两端（例如，商店1在x=0，商店2在x=1）。

# 3. **运输成本**：消费者购买商品的旅行成本与离商店的距离成比例，单位距离的成本为t。

# 4. **单位需求**：每个消费者只愿意购买一个单位的商品。

# 5. **成本函数**：每个商店提供单位产品的成本为c。

# 

# ### 霍特林模型中的均衡

# 

# 在Hotelling模型中，当两家商店的价格相等时，无差别消费者的位置位于两家商店的中间。如果一家商店提高价格，那么无差别消费者的位置会向价格较低的商店移动，直到两家商店的利润达到均衡。

# 

# ### Python代码示例

# 

# 以下是一个简化的Python代码示例，用于模拟Hotelling模型中的均衡价格计算。请注意，这个示例仅用于教学目的，并不包含完整的Hotelling模型的所有细节。

# 

# 

# 导入需要的库

import numpy as np

from scipy.optimize import minimize



# 定义参数

t = 0.1  # 单位距离的运输成本

c = 0.5  # 单位产品的成本



# 定义需求函数（简化版）

def demand(x, p1, p2):

    """

    计算商店1和商店2的需求。

    x是无差别消费者的位置。

    p1和p2分别是商店1和商店2的价格。

    """

    if x < 0.5:

        return x, 1 - x

    else:

        return 1 - x, x



# 定义利润函数（简化版）

def profit(prices, x):

    """

    计算给定价格和无差别消费者位置时的商店利润。

    prices是一个包含两个价格的元组：(p1, p2)

    """

    p1, p2 = prices

    d1, d2 = demand(x, p1, p2)

    return (p1 - c) * d1, (p2 - c) * d2



# 定义目标函数（最大化总利润）

def total_profit(prices):

    """

    计算两家商店的总利润，并返回负值以便使用minimize函数。

    """

    x = 0.5  # 简化版：假设无差别消费者始终位于中间

    p1_profit, p2_profit = profit(prices, x)

    return -(p1_profit + p2_profit)



# 使用scipy的minimize函数找到均衡价格

initial_guess = (1.0, 1.0)  # 初始猜测价格

result = minimize(total_profit, initial_guess, method='SLSQP')



# 输出结果

print(f"均衡价格为：商店1 {result.x[0]:.2f}，商店2 {result.x[1]:.2f}")

print(f"最大总利润为：-{result.fun:.2f}")



# 注意：这个示例是简化的，并且假设无差别消费者始终位于中间。

# 在完整的Hotelling模型中，无差别消费者的位置是内生决定的。
