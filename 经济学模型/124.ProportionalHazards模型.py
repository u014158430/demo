# 

# 

# **Proportional Hazards模型（Cox比例风险回归模型）详解与Python代码示例**

# 

# **一、模型解释**

# 

# Proportional Hazards模型，也称为Cox比例风险回归模型，是生存分析领域中的一种重要方法。该模型主要用于研究多个因素对生存时间的影响，特别是当数据中存在删失（即由于某些原因无法观察到确切的生存时间）时。Cox模型的一个关键假设是比例风险假设，即不同个体之间的风险比例不随时间变化。

# 

# 在Cox模型中，我们关注的是风险函数h(t)，它表示在给定时间t时，仍然存活的个体在下一瞬间死亡的风险。Cox模型假设这个风险函数可以分解为两部分：基线风险h0(t)（即没有外部变量影响时的风险）和外部变量x的效应（通过指数函数exp(βx)表示）。因此，Cox模型的基本形式可以表示为：

# 

# h(t|x) = h0(t) * exp(βx)

# 

# 其中，h0(t)是基线风险函数，β是协变量的偏回归系数，x是协变量向量。这个模型允许我们估计协变量对风险比例的影响，而不必对基线风险函数的具体形式做出假设。

# 

# **二、Python代码示例**

# 

# 下面是一个使用Python的lifelines库实现Cox比例风险回归模型的示例代码：

# 

# 

# 导入必要的库

import pandas as pd

from lifelines import CoxPHFitter



# 假设我们有一个名为df的DataFrame，其中包含生存时间、事件状态（1表示事件发生，0表示删失）和协变量

# 示例数据（实际使用时需要替换为真实数据）

data = {

    'T': [5, 3, 9, 8, 10, 7],  # 生存时间

    'E': [1, 1, 1, 0, 1, 1],    # 事件状态（1表示事件发生，0表示删失）

    'age': [40, 30, 60, 50, 70, 60],  # 协变量1：年龄

    'sex': [0, 1, 1, 0, 0, 1]   # 协变量2：性别（0表示女性，1表示男性）

}

df = pd.DataFrame(data)



# 初始化CoxPHFitter对象

cph = CoxPHFitter()



# 拟合模型

cph.fit(df, duration_col='T', event_col='E', regression_covariates=['age', 'sex'])



# 输出模型摘要，包括协变量的偏回归系数、标准误、z值和p值等

print(cph.summary)



# 预测新的数据点的风险

# 假设我们有一个新的数据点，年龄为55岁，性别为男性

new_data = pd.DataFrame({'age': [55], 'sex': [1]})

predicted_hazard = cph.predict_partial_hazard(new_data)

print("Predicted hazard for the new data point:", predicted_hazard)



# 预测新的数据点的生存函数

predicted_survival = cph.predict_survival_function(new_data)

print("Predicted survival function for the new data point:")

print(predicted_survival.iloc[:,0])  # 注意这里只取第一列，因为只有一个数据点
