# 

# 

# **纳什均衡的详细解释与Python代码示例**

# 

# 一、纳什均衡的详细解释

# 

# 纳什均衡是博弈论中的一个核心概念，由数学家约翰·纳什提出。它描述了在多人参与的博弈中，每个参与者都选择了一个策略，且在其他参与者策略不变的情况下，没有任何一个参与者可以通过改变自己的策略来获得更好的结果。换句话说，纳什均衡是一种稳定的策略组合，其中每个参与者都选择了针对其他参与者策略的最佳回应。

# 

# 纳什均衡可以分为纯策略纳什均衡和混合策略纳什均衡。纯策略纳什均衡是指每个参与者都选择了一个确定的策略，而混合策略纳什均衡则允许参与者以一定的概率选择不同的策略。混合策略纳什均衡在博弈论中尤为重要，因为它能够处理那些没有纯策略纳什均衡的博弈。

# 

# 二、Python代码示例

# 

# 以下是一个简单的Python代码示例，用于求解一个双人博弈的混合策略纳什均衡。假设我们有一个石头剪刀布的游戏，其中每个玩家都有三种选择：石头、剪刀和布。游戏的收益矩阵如下：

# 

import numpy as np



# 石头剪刀布游戏的收益矩阵

# 玩家1的行，玩家2的列

# 收益矩阵的解读：player1[i][j]表示当player1出i且player2出j时，player1的收益

payoff_matrix = np.array([[0, -1, 1],  # 玩家1出石头

                          [1, 0, -1],  # 玩家1出剪刀

                          [-1, 1, 0]]) # 玩家1出布



# 求解混合策略纳什均衡的函数

def nash_equilibrium(payoff_matrix):

    # 玩家数量（在这个例子中为2）

    num_players = payoff_matrix.shape[0]

    

    # 初始化概率分布（混合策略）

    mixed_strategy = np.zeros(num_players)

    

    # 对于每个玩家，求解其最优混合策略

    for i in range(num_players):

        # 复制收益矩阵并添加一列全1（用于后续求解线性方程组）

        A = np.concatenate((payoff_matrix[i, :].reshape(-1, 1), np.ones((1, num_players))), axis=0)

        # 初始化b向量（收益矩阵对应的列和加上1，表示概率之和为1）

        b = np.append(np.sum(payoff_matrix, axis=0), 1)

        

        # 使用numpy的线性代数求解器求解线性方程组

        x = np.linalg.solve(A, b)

        

        # 将求解得到的概率分布赋值给混合策略

        mixed_strategy[i] = x[:-1]

    

    # 返回所有玩家的混合策略

    return mixed_strategy



# 求解纳什均衡

nash_eq = nash_equilibrium(payoff_matrix)



# 打印结果

print("混合策略纳什均衡为：")

for i, strategy in enumerate(nash_eq):

    print(f"玩家{i+1}的策略概率分布为：{strategy}")



# 计算期望收益（在这个例子中，由于是对称的，所以期望收益为0）

expected_payoff = np.sum(np.dot(nash_eq, payoff_matrix) * nash_eq)

print(f"对应的期望收益为：{expected_payoff}")
