# 

# 

# **回归分析详解与Python代码示例**

# 

# 回归分析（Regression Analysis）是统计学中一种强大的工具，用于研究两个或多个变量之间的关系。具体来说，它试图确定一个或多个自变量（X）如何影响因变量（Y）的值。回归分析广泛应用于各种领域，如金融、医学、社会科学等，用于预测、解释和控制变量之间的关系。

# 

# ### 回归分析的分类

# 

# 1. **一元回归与多元回归**：一元回归涉及一个自变量和一个因变量，而多元回归涉及多个自变量和一个因变量。

# 2. **简单回归与多重回归**：简单回归研究一个因变量与一个自变量之间的关系，而多重回归研究一个因变量与多个自变量之间的关系。

# 3. **线性回归与非线性回归**：线性回归假设自变量和因变量之间存在线性关系，而非线性回归则不假设这种关系。

# 

# ### Python代码示例：简单线性回归

# 

# 以下是一个使用Python和`sklearn`库进行简单线性回归分析的示例。假设我们有一个数据集，其中包含两个变量：广告费用（X）和销售额（Y），我们想要了解广告费用如何影响销售额。

# 

# 

# 导入必要的库

import numpy as np

import matplotlib.pyplot as plt

from sklearn.model_selection import train_test_split

from sklearn.linear_model import LinearRegression

from sklearn.metrics import mean_squared_error, r2_score



# 假设的数据集（实际情况下，您会从文件或数据库中加载数据）

X = np.array([[1], [2], [3], [4], [5]]).astype(np.float64)  # 广告费用

y = np.array([2, 4, 5, 4, 5])  # 销售额



# 划分数据集为训练集和测试集（这里为了简单起见，我们直接使用全部数据作为训练集）

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)



# 创建线性回归模型

model = LinearRegression()



# 使用训练数据拟合模型

model.fit(X_train, y_train)



# 使用模型进行预测

y_pred = model.predict(X_test)



# 输出模型的截距和斜率

print(f'截距: {model.intercept_}')

print(f'斜率: {model.coef_}')



# 评估模型（这里仅使用测试集进行评估，实际情况下还会使用交叉验证等方法）

mse = mean_squared_error(y_test, y_pred)

r2 = r2_score(y_test, y_pred)

print(f'均方误差: {mse}')

print(f'R^2 分数: {r2}')



# 可视化结果

plt.scatter(X_train, y_train, color='blue', label='训练数据')

plt.plot(X_test, y_pred, color='red', linewidth=2, label='预测')

plt.xlabel('广告费用')

plt.ylabel('销售额')

plt.legend()

plt.show()
