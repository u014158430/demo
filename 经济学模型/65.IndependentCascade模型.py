# 

# 

# 独立级联模型（Independent Cascade Model，简称IC模型）是一种在社交网络分析中广泛使用的信息传播模型。它模拟了信息在社交网络中的传播过程，特别是当信息从一个节点（用户）传播到其邻居节点（用户的朋友或关注者）时的情况。下面我将对独立级联模型进行详细解释，并提供一个基于Python的代码示例。

# 

# ### 独立级联模型解释

# 

# 在独立级联模型中，社交网络被抽象为一个图，图中的节点代表用户，边代表用户之间的关系（如朋友关系、关注关系等）。每个节点都有两种状态：激活状态（已接受信息）和未激活状态（未接受信息）。模型开始时，只有一小部分节点处于激活状态，这些节点被称为种子节点（seed nodes）。

# 

# 在任意时刻t，如果一个节点u在t-1时刻被激活，那么它将以一定的概率尝试激活其所有未激活的邻居节点v。这个激活过程是相互独立的，即节点u对不同的邻居节点v的激活尝试是互不影响的。如果节点v在t时刻被至少一个邻居节点成功激活，那么它将在t时刻变为激活状态。每个节点只有一次机会去激活其邻居节点，无论成功与否，在t+1时刻它将不再具备激活能力。

# 

# 这个过程会一直持续下去，直到没有更多的节点可以被激活为止。最终，处于激活状态的节点数量反映了信息在社交网络中的传播范围。

# 

# ### Python代码示例

# 

# 下面是一个基于Python的简单独立级联模型实现示例：

# 

# 

import numpy as np



class IndependentCascadeModel:

    def __init__(self, graph, seed_nodes, p_activation):

        """

        初始化独立级联模型

        :param graph: 社交网络图，以邻接矩阵形式表示

        :param seed_nodes: 种子节点集合

        :param p_activation: 激活概率

        """

        self.graph = graph

        self.seed_nodes = set(seed_nodes)

        self.p_activation = p_activation

        self.activated_nodes = set(seed_nodes)  # 初始化激活节点集合



    def activate_neighbors(self, node):

        """

        尝试激活节点的邻居节点

        :param node: 当前节点

        """

        neighbors = np.where(self.graph[node] == 1)[0]  # 获取邻居节点

        for neighbor in neighbors:

            if neighbor not in self.activated_nodes:  # 如果邻居节点未被激活

                if np.random.rand() < self.p_activation:  # 以一定概率激活邻居节点

                    self.activated_nodes.add(neighbor)



    def simulate(self):

        """

        模拟信息传播过程

        """

        while True:

            new_activations = False  # 标记本轮是否有新的节点被激活

            for node in self.activated_nodes:  # 遍历当前激活节点集合

                self.activate_neighbors(node)  # 尝试激活邻居节点

                if len(self.activated_nodes) > len(self.activated_nodes - {node}):  # 如果有新的节点被激活

                    new_activations = True

            if not new_activations:  # 如果没有新的节点被激活，则结束模拟

                break



# 示例用法

# 假设我们有一个4个节点的社交网络，邻接矩阵表示如下：

#   0 1 1 0

#   1 0 1 1

#   1 1 0 1

#   0 1 1 0

graph = np.array([

    [0, 1, 1, 0],

    [1, 0, 1, 1],

    [1, 1, 0, 1],

    [0, 1, 1, 0]

], dtype=int)



# 假设节点0是种子节点

seed_nodes = [0]



# 假设激活概率为0.5

p_activation = 0.5



# 初始化并模拟独立级联模型

model = IndependentCascadeModel(graph, seed_nodes, p_activation)

model.simulate()



# 输出最终激活的节点集合

print("最终激活的节点集合：", model.activated_nodes)
