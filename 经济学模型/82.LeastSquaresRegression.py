# 

# 

# **最小二乘法回归（Least-Squares Regression）详解与Python代码示例**

# 

# 在统计学和数据分析中，最小二乘法回归（Least-Squares Regression，简称LSR）是一种常用的预测建模技术。其核心思想是通过最小化预测值与实际观测值之间差的平方和（即残差平方和）来估计模型的参数，从而找到一条最佳拟合数据的直线或曲线。

# 

# ### 最小二乘法回归的基本原理

# 

# 最小二乘法回归的基本假设是：因变量（响应变量）与自变量（解释变量）之间存在线性关系。通过收集一组观测数据（x, y），我们可以尝试拟合一个线性模型 y = ax + b，其中a是斜率，b是截距。为了找到最佳的a和b，我们需要最小化所有观测点上的预测值与实际值之间的残差平方和。

# 

# 数学上，最小二乘法回归的目标函数可以表示为：

# \[ J(a, b) = \sum_{i=1}^{n} (y_i - (ax_i + b))^2 \]

# 其中，\(y_i\) 是第i个观测点的实际值，\(x_i\) 是对应的自变量值，n是观测点的数量。我们的目标是找到使 \(J(a, b)\) 最小的 \(a\) 和 \(b\)。

# 

# ### Python代码示例

# 

# 下面是一个使用Python和NumPy库实现最小二乘法回归的示例代码：

# 

# 

import numpy as np



# 假设我们有一组观测数据

x_data = np.array([1, 2, 3, 4, 5])  # 自变量

y_data = np.array([2, 3, 5, 7, 11])  # 因变量



# 最小二乘法回归的实现

def least_squares_regression(x, y):

    # 计算x的均值和方差

    x_mean = np.mean(x)

    x_var = np.var(x)

    

    # 计算斜率a和截距b

    a = (np.sum((x - x_mean) * (y - np.mean(y))) / x_var)

    b = np.mean(y) - a * x_mean

    

    # 返回斜率a和截距b

    return a, b



# 调用函数并打印结果

a, b = least_squares_regression(x_data, y_data)

print(f"斜率a: {a}, 截距b: {b}")



# 使用得到的参数a和b进行预测

x_predict = np.array([6, 7])

y_predict = a * x_predict + b

print(f"当x为{x_predict}时，预测的y值为：{y_predict}")
