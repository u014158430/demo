# 

# 

# **帕累托分析（Pareto Analysis）详解与Python代码示例**

# 

# 一、帕累托分析概述

# 

# 帕累托分析，也被称为帕累托法则或80/20原则，是一种广泛应用于各种领域的决策分析方法。它基于一个观察：在许多情况下，大约20%的原因会导致80%的问题发生。帕累托分析通过识别这些“关键的少数”原因，帮助决策者优化资源分配，集中力量解决主要问题，从而实现效益最大化。

# 

# 在质量管理、项目管理、市场营销、财务管理等领域，帕累托分析都发挥着重要作用。例如，在市场营销中，通过帕累托分析可以确定哪些产品或服务贡献了大部分销售额，从而优化产品组合和营销策略。

# 

# 二、Python代码示例

# 

# 下面是一个使用Python进行帕累托分析的示例代码，假设我们有一组产品销售数据，需要找出哪些产品贡献了大部分销售额。

# 

# 

import pandas as pd

import matplotlib.pyplot as plt



# 假设我们有以下产品销售数据（销售额）

sales_data = {'Product': ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'],

              'Sales': [1000, 2000, 3000, 4000, 500, 600, 700, 800, 900, 100]}



# 将数据转换为DataFrame

df = pd.DataFrame(sales_data)



# 按照销售额降序排序

df_sorted = df.sort_values(by='Sales', ascending=False)



# 计算累计销售额占比

df_sorted['Cumulative_Percentage'] = df_sorted['Sales'].cumsum() / df_sorted['Sales'].sum() * 100



# 找出累计占比超过80%的第一个产品

threshold = 80

cumulative_sum = 0

for index, row in df_sorted.iterrows():

    cumulative_sum += row['Sales']

    if (cumulative_sum / df['Sales'].sum()) * 100 >= threshold:

        key_product = row['Product']

        break



# 绘制帕累托图

plt.figure(figsize=(10, 6))

plt.bar(df_sorted['Product'], df_sorted['Sales'], color='skyblue', alpha=0.7)

plt.xlabel('Product')

plt.ylabel('Sales')

plt.title('Pareto Analysis of Product Sales')



# 绘制累计占比线

plt.plot(df_sorted['Product'], df_sorted['Cumulative_Percentage'], marker='o', linestyle='-', color='red', label='Cumulative Percentage')



# 标记关键产品

plt.axvline(x=df_sorted[df_sorted['Product'] == key_product].index.min(), color='black', linestyle='--', label='Key Product Threshold')

plt.text(df_sorted[df_sorted['Product'] == key_product].index.min() - 0.2, 

         df_sorted[df_sorted['Product'] == key_product]['Cumulative_Percentage'].values[0] + 2, 

         f'Key Product: {key_product}', 

         color='black', 

         rotation=90)



# 添加图例

plt.legend()



# 显示图形

plt.show()
