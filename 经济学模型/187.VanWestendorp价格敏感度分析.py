# 

# 

# **Van Westendorp价格敏感度分析（PSM）详解与Python代码示例**

# 

# **一、Van Westendorp价格敏感度分析（PSM）详解**

# 

# Van Westendorp价格敏感度分析（Price Sensitivity Meter，简称PSM）是一种基于消费者调查的市场研究方法，旨在评估消费者对产品或服务价格变化的反应程度。该方法由荷兰经济学家Herman Simon和荷兰市场研究学者Peter van Westendorp在1976年共同提出，通过一系列问题来收集消费者对价格的看法，从而帮助企业确定最佳定价策略。

# 

# PSM的核心在于通过四个关键问题来测量消费者的价格敏感度：

# 

# 1. **太便宜的价格**：询问消费者认为产品价格低于多少时会怀疑其质量。

# 2. **物有所值的价格**：询问消费者认为产品价格在什么范围内是物有所值、价格公正的。

# 3. **贵的价格**：询问消费者认为产品价格高于多少时会觉得有些贵，但可能仍会考虑购买。

# 4. **太贵的价格**：询问消费者认为产品价格高于多少时会觉得过于昂贵，无法接受。

# 

# 通过对这四个问题的回答进行统计分析，可以绘制出价格需求弹性曲线，并确定产品的合适价格区间。这个区间通常位于消费者认为“物有所值”和“贵的价格”之间，既能保证企业的利润，又能满足消费者的需求。

# 

# **二、Python代码示例**

# 

# 以下是一个使用Python进行Van Westendorp价格敏感度分析的简单示例。请注意，这个示例假设你已经通过某种方式（如问卷调查）收集到了消费者的回答数据。

# 

# 

import pandas as pd

import numpy as np

import matplotlib.pyplot as plt



# 假设我们有一个DataFrame，其中包含消费者对四个问题的回答

# 每一列代表一个问题的回答，单位是元

data = pd.DataFrame({

    'Too_Cheap': [50, 60, 40, 70, 55],  # 太便宜的价格

    'Value_for_Money': [70, 80, 65, 90, 75],  # 物有所值的价格

    'Expensive': [100, 110, 95, 120, 105],  # 贵的价格

    'Too_Expensive': [150, 160, 140, 170, 155]  # 太贵的价格

})



# 计算累积百分比

data_cum = data.cumsum(axis=0) / len(data) * 100



# 绘制价格需求弹性曲线

plt.figure(figsize=(10, 6))



# 太便宜的价格曲线（向下累积百分比）

plt.plot(data_cum['Too_Cheap'], label='Too Cheap (Downward Cumulative %)', linestyle='--')



# 物有所值的价格曲线（向上累积百分比）

plt.plot(data_cum['Value_for_Money'][::-1], label='Value for Money (Upward Cumulative %)', linestyle='-')



# 贵的价格曲线（向下累积百分比）

plt.plot(data_cum['Expensive'], label='Expensive (Downward Cumulative %)', linestyle='--')



# 太贵的价格曲线（向上累积百分比）

plt.plot(data_cum['Too_Expensive'][::-1], label='Too Expensive (Upward Cumulative %)', linestyle='-')



# 设置图例和标题

plt.legend()

plt.title('Price Sensitivity Meter (PSM) Analysis')

plt.xlabel('Price (in Yuan)')

plt.ylabel('Cumulative Percentage')



# 显示图形

plt.show()



# 注意：此代码仅用于演示目的，实际分析时可能需要更复杂的统计和图形处理
