# 

# 

# **贝叶斯网络详解与Python代码示例**

# 

# 贝叶斯网络（Bayesian Network），又称信度网络或是有向无环图模型（Directed Acyclic Graphical Model），是一种基于概率推理的图形化网络，用于表示变量之间的依赖关系和概率分布。它起源于托马斯·贝叶斯（Thomas Bayes）的贝叶斯定理，由Judea Pearl在1988年提出，并逐渐成为处理不确定性和复杂依赖关系问题的有效工具。

# 

# ### 贝叶斯网络的基本原理

# 

# 贝叶斯网络由一个有向无环图（DAG）构成，图中的节点代表随机变量，节点之间的有向边表示变量之间的依赖关系。每个节点都附有一个概率分布表，用于描述该节点在其父节点给定条件下的概率分布。没有父节点的节点则使用先验概率进行描述。

# 

# 贝叶斯网络的核心思想是利用已知的变量信息，通过概率推理来计算未知变量的概率分布。这种推理过程可以基于贝叶斯定理进行，从而实现对复杂系统的建模和预测。

# 

# ### Python代码示例

# 

# 下面是一个使用Python和pgmpy库实现贝叶斯网络的简单示例。在这个示例中，我们将构建一个用于预测学生是否会被推荐读研究生的贝叶斯网络模型。

# 

# 

# 导入必要的库

from pgmpy.models import BayesianModel

from pgmpy.factors.discrete import TabularCPD



# 定义贝叶斯网络结构

# 节点包括：智力（Intelligence, I）、成绩（Grade, G）、推荐信（Letter, L）

model = BayesianModel([('I', 'G'), ('G', 'L')])



# 定义节点的概率分布

# 智力（I）的先验概率

cpd_i = TabularCPD(variable='I', variable_card=2, values=[[0.3], [0.7]])  # 假设智力高的概率为0.7

model.add_cpds(cpd_i)



# 成绩（G）的条件概率分布（依赖于智力I）

cpd_g = TabularCPD(variable='G', variable_card=3, 

                    values=[[0.1, 0.9, 0], [0.2, 0.7, 0.1]], 

                    evidence=['I'], evidence_card=[2])  # 假设智力高时成绩好的概率为0.9

model.add_cpds(cpd_g)



# 推荐信（L）的条件概率分布（依赖于成绩G）

cpd_l = TabularCPD(variable='L', variable_card=2, 

                    values=[[0.1, 0.9], [0.4, 0.6], [0.99, 0.01]], 

                    evidence=['G'], evidence_card=[3])  # 假设成绩好时获得推荐信的概率高

model.add_cpds(cpd_l)



# 查询给定智力水平下获得推荐信的概率

from pgmpy.inference import VariableElimination

inference = VariableElimination(model)

query = inference.query(['L'], evidence={'I': 1})  # 假设智力高（I=1）

print(query)
