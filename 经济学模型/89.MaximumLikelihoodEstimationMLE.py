# 

# 

# **最大似然估计（Maximum Likelihood Estimation, MLE）详解与Python代码示例**

# 

# 一、最大似然估计（MLE）详解

# 

# 最大似然估计是一种在统计学中广泛应用的参数估计方法。它的核心思想是在给定一组观测数据的情况下，寻找能够最大化这组数据出现概率的参数值。换句话说，最大似然估计试图找到最能够“解释”或“拟合”观测数据的参数值。

# 

# 在概率论中，似然函数是一个关于模型参数的函数，它表示在给定的参数值下，观测数据出现的概率。最大似然估计就是寻找使似然函数取最大值的那个参数值。

# 

# 具体来说，假设我们有一个概率模型，其参数为θ，观测数据为X。似然函数L(θ|X)表示在给定参数θ的情况下，观测数据X出现的概率。最大似然估计就是求解使L(θ|X)达到最大值的θ值。

# 

# 为了求解方便，我们通常对似然函数取对数，得到对数似然函数。因为对数函数是单调增函数，所以对数似然函数的最大值点就是似然函数的最大值点。

# 

# 二、Python代码示例

# 

# 下面是一个使用Python实现最大似然估计的示例代码，假设观测数据服从正态分布N(μ, σ²)，我们要估计的是正态分布的均值μ和标准差σ。

# 

# 

import numpy as np

from scipy.optimize import minimize

from scipy.stats import norm



# 假设观测数据

data = np.array([1.2, 2.3, 3.4, 4.5, 5.6, 6.7, 7.8, 8.9])



# 定义负对数似然函数（因为scipy.optimize.minimize默认求最小值）

def neg_log_likelihood(params, data):

    mu, sigma = params

    return -np.sum(np.log(norm.pdf(data, mu, sigma)))



# 初始参数值

initial_params = [np.mean(data), np.std(data)]



# 使用scipy.optimize.minimize求解最小负对数似然值对应的参数值

result = minimize(neg_log_likelihood, initial_params, args=(data,))



# 输出结果

mu_hat, sigma_hat = result.x

print(f"Estimated mean (μ): {mu_hat}")

print(f"Estimated standard deviation (σ): {sigma_hat}")



# 验证结果（可选）

print(f"True log-likelihood with estimated parameters: {np.sum(np.log(norm.pdf(data, mu_hat, sigma_hat)))}")
