# 

# 

# **Mean-Variance模型详解与Python代码示例**

# 

# **一、模型详解**

# 

# Mean-Variance模型，也称为均值-方差模型，是现代投资组合理论的基石，由经济学家哈里·马科维茨（Harry Markowitz）在1952年提出。该模型的核心思想是，在给定风险水平下最大化投资组合的预期收益，或在给定预期收益下最小化投资组合的风险。这里的“风险”通常用收益率的方差或标准差来衡量，而“预期收益”则是基于历史数据或预测得出的。

# 

# 在Mean-Variance模型中，投资者面临的核心问题是如何在众多可能的证券组合中选择一个最优的组合。这个最优组合需要满足两个条件：一是预期收益尽可能高，二是风险尽可能低。为了达到这个目标，投资者需要综合考虑各个证券的预期收益、风险以及它们之间的相关性（通常用协方差来衡量）。

# 

# 具体来说，Mean-Variance模型通过以下步骤来求解最优投资组合：

# 

# 1. **数据准备**：收集各个证券的历史收益率数据，并计算它们的预期收益、方差和协方差。

# 2. **模型建立**：根据预期收益、方差和协方差建立数学模型，表示投资组合的预期收益和风险。

# 3. **求解优化问题**：在给定的约束条件下（如投资预算、不允许卖空等），求解使投资组合风险最小或预期收益最大的证券组合比例。

# 

# **二、Python代码示例**

# 

# 下面是一个使用Python和NumPy库实现的Mean-Variance模型的简单示例。假设我们已经有了一个包含多只股票历史收益率的CSV文件（`stock_returns.csv`）。

# 

# 

import numpy as np

import pandas as pd



# 读取股票收益率数据

returns = pd.read_csv("stock_returns.csv", index_col=0)



# 计算预期收益（均值）

mean_returns = returns.mean()



# 计算协方差矩阵

cov_matrix = returns.cov()



# 设定预期收益目标（可以根据实际情况调整）

target_return = 0.05  # 假设目标预期收益率为5%



# 设定投资比例向量x的初始值（等权重分配）

num_assets = len(mean_returns)

x = np.ones(num_assets) / num_assets



# 使用优化库（如SciPy）来求解最优投资比例

# 注意：这里为了简化示例，没有直接使用SciPy库，而是用了一个简单的迭代方法

# 在实际应用中，应该使用更高效的优化算法



# 迭代求解过程（这里仅作为示例，实际中应使用更复杂的优化算法）

while True:

    # 计算当前投资组合的预期收益和风险

    portfolio_return = np.dot(mean_returns, x)

    portfolio_risk = np.sqrt(np.dot(x.T, np.dot(cov_matrix, x)))

    

    # 如果当前投资组合的预期收益达到或超过目标预期收益，则停止迭代

    if portfolio_return >= target_return:

        break

    

    # 否则，根据一定的策略调整投资比例（这里仅作为示例，实际中应有更复杂的策略）

    # 例如，可以简单地按照预期收益与风险的比值来调整投资比例

    x = x * (mean_returns / portfolio_risk)

    x = x / np.sum(x)  # 归一化投资比例



# 输出最优投资组合的预期收益和风险

print("最优投资组合的预期收益:", portfolio_return)

print("最优投资组合的风险:", portfolio_risk)

print("最优投资比例:", x)
