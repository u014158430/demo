# 

# 

# **主成分分析（PCA）详解与Python代码示例**

# 

# **一、主成分分析（PCA）详解**

# 

# 主成分分析（Principal Component Analysis，简称PCA）是一种在数据分析和降维领域广泛应用的统计技术。其核心思想是将原始数据通过线性变换，转化为一个新的坐标系，这个新坐标系由数据中的主成分构成，这些主成分是数据中方差最大的线性组合。通过这种方式，PCA可以帮助我们减少数据的维度，去除冗余信息，提取关键特征，从而更好地理解和分析数据。

# 

# 具体来说，PCA的步骤如下：

# 

# 1. **数据标准化**：对原始数据进行标准化处理，使得每个特征具有零均值和单位方差。这是为了确保每个特征在后续的分析中具有相同的权重。

# 2. **计算协方差矩阵**：协方差矩阵描述了数据集中各个特征之间的相关性。PCA的目标就是找到一组正交的单位向量（主成分向量），使得数据在这些主成分方向上的投影具有最大的方差。

# 3. **计算协方差矩阵的特征值和特征向量**：特征值表示了对应主成分方向上的方差大小，而特征向量则代表了主成分的方向。

# 4. **选择主成分**：根据特征值的大小，选择前k个主成分，其中k是希望降维后的维度数。通常选择特征值较大的前几个主成分，因为它们包含了数据中的主要信息。

# 5. **数据转换**：将数据映射到选定的主成分上，得到降维后的数据。

# 

# **二、Python代码示例**

# 

# 下面是一个使用Python和scikit-learn库进行PCA降维的示例代码：

# 

# 

# 导入必要的库

from sklearn.decomposition import PCA

from sklearn.preprocessing import StandardScaler

import numpy as np

import matplotlib.pyplot as plt



# 示例数据

X = np.array([[1, 2], [5, 8], [1.5, 1.8], [8, 8], [1, 0.6], [9, 11]])



# 数据标准化

scaler = StandardScaler()

X_scaled = scaler.fit_transform(X)



# 初始化PCA对象，设置目标维度为2

pca = PCA(n_components=2)



# 对数据进行PCA降维

X_pca = pca.fit_transform(X_scaled)



# 输出降维后的数据

print("降维后的数据:")

print(X_pca)



# 可视化原始数据和降维后的数据

plt.figure(figsize=(10, 5))



# 原始数据散点图

plt.subplot(1, 2, 1)

plt.scatter(X[:, 0], X[:, 1], alpha=0.8, label='原始数据')

plt.xlabel('特征1')

plt.ylabel('特征2')

plt.legend()



# 降维后数据散点图

plt.subplot(1, 2, 2)

plt.scatter(X_pca[:, 0], X_pca[:, 1], alpha=0.8, label='PCA降维后数据')

plt.xlabel('第一主成分')

plt.ylabel('第二主成分')

plt.legend()



plt.tight_layout()

plt.show()
