# 

# 

# ### 解释

# 

# `M/G/1` 队列模型是排队论（Queueing Theory）中的一个经典模型，用于描述和分析一个服务系统的性能。在这个模型中，`M` 表示顾客（或称为任务、请求）的到达服从泊松分布（Poisson Distribution），即顾客到达的时间间隔是随机的，且服从指数分布；`G` 表示服务时间服从一般分布（General Distribution），即服务时间可以是任何分布，如正态分布、指数分布等；`1` 表示服务台的数量为一个。

# 

# 这个模型通常用于描述诸如电话交换机、计算机服务器、银行柜台等只有一个服务台且顾客到达和服务时间均为随机的系统。通过该模型，我们可以计算出系统的各种性能指标，如平均队长、平均等待时间、服务台的利用率等。

# 

# ### Python代码示例

# 

# 以下是一个简化的Python代码示例，用于模拟`M/G/1`队列模型。由于服务时间服从一般分布，这里我们假设它服从正态分布作为示例。请注意，这个模拟是简化的，真实的`M/G/1`模型可能需要更复杂的数学方法和工具来精确分析。

# 

# 

import numpy as np

import matplotlib.pyplot as plt

from scipy.stats import expon, norm

from collections import deque



# 参数设置

lambda_ = 2.0  # 顾客到达率（泊松分布参数）

mu_ = 1.5  # 服务率（假设服务时间服从均值为1/mu_的正态分布）

sim_time = 100  # 模拟时间（单位：时间单位）

service_time_mean = 1.0 / mu_  # 服务时间均值

service_time_std = 0.2  # 服务时间标准差



# 初始化队列和变量

queue = deque()

time = 0

customers_served = 0

waiting_times = []



# 模拟过程

while time < sim_time:

    # 顾客到达（泊松分布）

    if np.random.rand() < lambda_ * np.exp(-lambda_ * time):

        arrival_time = time

        queue.append(arrival_time)

    

    # 如果队列中有顾客且服务台空闲，开始服务

    if queue and (not customers_served or time >= queue[0] + norm.rvs(loc=service_time_mean, scale=service_time_std)):

        departure_time = time

        waiting_time = departure_time - queue.popleft()

        waiting_times.append(waiting_time)

        customers_served += 1

        time = departure_time

    else:

        # 没有顾客到达或服务台忙碌，时间推进一小步

        time += 0.01



# 绘制等待时间直方图

plt.hist(waiting_times, bins=30, density=True, alpha=0.6, color='g')

plt.xlabel('Waiting Time')

plt.ylabel('Probability Density')

plt.title('Histogram of Waiting Times in M/G/1 Queue')

plt.show()



# 输出性能指标（这里只计算了平均等待时间）

avg_waiting_time = np.mean(waiting_times)

print(f"Average Waiting Time: {avg_waiting_time:.2f} time units")
