# 

# 

# **Pearson相关系数解释与Python代码示例**

# 

# **一、Pearson相关系数解释**

# 

# Pearson相关系数，也称为皮尔逊相关系数，是一种用于衡量两个变量之间线性相关程度的统计量。其值域为-1到1之间，其中：

# 

# - 当相关系数为1时，表示两个变量完全正相关，即一个变量随着另一个变量的增加而增加，减少而减少。

# - 当相关系数为-1时，表示两个变量完全负相关，即一个变量随着另一个变量的增加而减少，减少而增加。

# - 当相关系数为0时，表示两个变量之间没有线性关系。

# 

# 需要注意的是，Pearson相关系数仅适用于衡量线性关系，对于非线性关系可能无法准确反映。同时，它要求数据满足正态分布，并且样本量足够大时结果才较为可靠。

# 

# **二、Python代码示例**

# 

# 下面是一个使用Python计算Pearson相关系数的示例代码，这里我们使用`numpy`库生成一些模拟数据，并使用`scipy.stats`库中的`pearsonr`函数来计算相关系数。

# 

# 

import numpy as np

from scipy.stats import pearsonr



# 生成模拟数据

# 假设我们有两个变量x和y，它们之间存在某种线性关系

np.random.seed(0)  # 设置随机种子以便结果可复现

x = np.random.randn(100)  # 生成100个符合标准正态分布的随机数作为x

y = 2 * x + 1 + np.random.randn(100) * 0.5  # 生成与x线性相关的y，并加入一些噪声



# 计算Pearson相关系数

# pearsonr函数返回两个值：相关系数和p值（用于检验相关性的显著性）

correlation, _ = pearsonr(x, y)



# 输出结果

print(f"Pearson相关系数: {correlation}")



# 根据相关系数解释结果

if correlation > 0.8:

    print("两个变量之间存在很强的正相关关系")

elif correlation < -0.8:

    print("两个变量之间存在很强的负相关关系")

elif abs(correlation) < 0.2:

    print("两个变量之间的线性关系很弱")

else:

    print("两个变量之间存在一定程度的线性关系")



# 绘制散点图以可视化数据和相关关系

import matplotlib.pyplot as plt



plt.scatter(x, y, label='Data Points')

plt.xlabel('x')

plt.ylabel('y')

plt.title(f'Pearson Correlation: {correlation:.2f}')

plt.legend()

plt.show()
