# 

# 

# **Forward and Backward Scheduling 解释与Python代码示例**

# 

# 在项目管理、制造流程以及供应链管理中，Forward Scheduling（正向排程）和Backward Scheduling（反向排程）是两种常用的时间规划方法。Forward Scheduling通常是从项目的开始日期出发，按照预定的任务顺序和持续时间，推算出项目的结束日期。而Backward Scheduling则是从项目的结束日期或某个关键里程碑出发，逆向推算出各个任务的最迟开始日期。

# 

# ### Forward Scheduling 解释

# 

# 正向排程是一种线性的时间规划方法，它基于项目的开始日期和每个任务的预计持续时间，来安排任务的执行顺序和完成时间。这种方法适用于任务之间有明确依赖关系，且需要严格按照时间顺序执行的项目。

# 

# ### Backward Scheduling 解释

# 

# 反向排程则是一种逆向思维的时间规划方法，它从项目的结束日期或某个关键里程碑出发，逆向推算出各个任务的最迟开始日期。这种方法适用于需要确保项目在特定日期前完成，或者需要优先安排关键任务的情况。

# 

# ### Python 代码示例

# 

# 下面是一个简单的Python代码示例，用于演示Forward Scheduling和Backward Scheduling的基本思想。

# 

# 

# 假设我们有一个项目，包含四个任务，每个任务都有预计的持续时间

tasks = [

    {'name': 'Task 1', 'duration': 5},

    {'name': 'Task 2', 'duration': 3},

    {'name': 'Task 3', 'duration': 7},

    {'name': 'Task 4', 'duration': 2}

]



# Forward Scheduling

def forward_scheduling(start_date, tasks):

    current_date = start_date

    schedule = []

    for task in tasks:

        task_start_date = current_date

        task_end_date = current_date + timedelta(days=task['duration'])

        schedule.append({'task': task['name'], 'start': task_start_date, 'end': task_end_date})

        current_date = task_end_date

    return schedule



# 假设项目从2023年1月1日开始

start_date = date(2023, 1, 1)

forward_schedule = forward_scheduling(start_date, tasks)

print("Forward Scheduling:")

for item in forward_schedule:

    print(f"Task: {item['task']}, Start: {item['start']}, End: {item['end']}")



# Backward Scheduling

def backward_scheduling(end_date, tasks):

    current_date = end_date

    schedule = []

    for task in reversed(tasks):  # 反向遍历任务列表

        task_end_date = current_date

        task_start_date = current_date - timedelta(days=task['duration'])

        schedule.append({'task': task['name'], 'start': task_start_date, 'end': task_end_date})

        current_date = task_start_date

    # 反转列表以恢复任务顺序

    schedule.reverse()

    return schedule



# 假设项目需要在2023年1月20日完成

end_date = date(2023, 1, 20)

backward_schedule = backward_scheduling(end_date, tasks)

print("\nBackward Scheduling:")

for item in backward_schedule:

    print(f"Task: {item['task']}, Start: {item['start']}, End: {item['end']}")
