# 

# 

# **Whittaker-Shannon采样定理详解与Python代码示例**

# 

# **一、Whittaker-Shannon采样定理概述**

# 

# Whittaker-Shannon采样定理，也被称为奈奎斯特-香农采样定理或香农采样定理，是信息论和通信领域中的一个重要定理。它揭示了连续时间信号与离散时间信号之间的转换关系，并给出了在采样过程中避免信息丢失的条件。简单来说，该定理指出，为了从离散样本中无失真地恢复原始连续信号，采样频率必须至少为信号最高频率的两倍。

# 

# **二、定理详解**

# 

# 假设我们有一个连续时间信号`x(t)`，其频谱在`(-W, W)`范围内是有限的，即信号的最高频率为`W`。根据Whittaker-Shannon采样定理，如果我们以采样频率`f_s`（满足`f_s >= 2W`）对信号进行采样，那么可以通过这些离散样本完全恢复原始信号。

# 

# 这里的关键在于采样频率`f_s`的选择。如果采样频率过低（小于信号最高频率的两倍），则会出现混叠现象，导致无法从离散样本中恢复原始信号。混叠现象是指高频信号被错误地解释为低频信号，从而导致信号失真。

# 

# **三、Python代码示例**

# 

# 下面是一个使用Python和NumPy库来演示Whittaker-Shannon采样定理的简单示例。我们将生成一个正弦波信号，并使用不同的采样频率对其进行采样，然后观察采样后的信号是否能够无失真地恢复原始信号。

# 

# 

import numpy as np

import matplotlib.pyplot as plt



# 参数设置

T = 1.0  # 信号周期

f = 5.0  # 信号频率

fs_low = 5.0  # 低采样频率

fs_high = 10.0  # 高采样频率

t = np.arange(0, T, 1/fs_high)  # 时间向量，使用高采样频率以确保准确性



# 生成原始信号

x = np.sin(2 * np.pi * f * t)



# 低采样频率采样

t_low = np.arange(0, T, 1/fs_low)

x_low = np.sin(2 * np.pi * f * t_low)



# 绘制原始信号和低采样频率采样后的信号

plt.figure(figsize=(10, 6))

plt.subplot(2, 1, 1)

plt.plot(t, x, label='Original Signal')

plt.title('Original Signal and Low Sampling Rate')

plt.xlabel('Time (s)')

plt.ylabel('Amplitude')

plt.legend()



plt.subplot(2, 1, 2)

plt.plot(t_low, x_low, 'o', label='Sampled Signal (Low Rate)')

plt.title('Sampled Signal with Low Sampling Rate')

plt.xlabel('Time (s)')

plt.ylabel('Amplitude')

plt.legend()



plt.tight_layout()

plt.show()
