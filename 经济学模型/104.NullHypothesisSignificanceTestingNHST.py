# 

# 

# **Null Hypothesis Significance Testing（NHST）详解与Python代码示例**

# 

# **一、Null Hypothesis Significance Testing（NHST）详解**

# 

# Null Hypothesis Significance Testing（NHST），即零假设显著性检验，是统计学中用于检验某个假设是否成立的一种方法。在NHST中，我们首先设定一个零假设（H0），通常这个假设是预期没有差异或没有效果的假设。然后，我们利用样本数据来计算一个统计量，如t值、F值等，并基于这个统计量计算出一个p值。p值表示在零假设为真的前提下，观察到当前数据或更极端数据出现的概率。

# 

# 如果p值小于某个预设的显著性水平（如0.05），我们就认为观察到的数据在统计上是显著的，即拒绝零假设，认为存在差异或效果。反之，如果p值大于显著性水平，则接受零假设，认为没有差异或效果。

# 

# NHST在科学研究、医学实验、市场调查等领域有着广泛的应用，它可以帮助我们基于样本数据对总体做出推断。

# 

# **二、Python代码示例**

# 

# 下面是一个使用Python进行t检验（一种常见的NHST方法）的示例代码：

# 

# 

# 导入必要的库

import numpy as np

from scipy import stats



# 假设我们有两个独立样本的数据

group1 = np.array([1, 2, 3, 4, 5])  # 第一组数据

group2 = np.array([6, 7, 8, 9, 10])  # 第二组数据



# 进行两独立样本t检验

# equal_var 参数指定是否假设两个总体的方差相等，这里我们假设不等

t_statistic, p_value = stats.ttest_ind(group1, group2, equal_var=False)



# 输出t值和p值

print("t statistic:", t_statistic)

print("p value:", p_value)



# 判断结果

if p_value < 0.05:

    print("The difference between the two groups is statistically significant.")

    print("We reject the null hypothesis.")

else:

    print("The difference between the two groups is not statistically significant.")

    print("We fail to reject the null hypothesis.")



# 注释：

# 1. 我们首先导入了numpy和scipy.stats库，这两个库在Python中提供了丰富的统计计算功能。

# 2. 然后，我们创建了两个numpy数组作为两个独立样本的数据。

# 3. 使用scipy.stats.ttest_ind函数进行两独立样本t检验。该函数返回两个值：t统计量和p值。

# 4. 我们将t统计量和p值打印出来，以便查看。

# 5. 最后，我们根据p值的大小来判断结果。如果p值小于0.05，则拒绝零假设，认为两组数据之间存在显著差异；否则，接受零假设，认为两组数据之间不存在显著差异。
