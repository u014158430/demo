# 

# 

# **排队论（Queuing Theory）详解与Python代码示例**

# 

# 排队论，又称为随机服务系统理论，是运筹学的一个重要分支，它主要研究的是服务系统中顾客的到达、排队、接受服务以及离开等过程的数学规律。排队论广泛应用于各种需要处理随机聚散现象和随机服务系统工作过程的领域，如交通、通信、计算机网络、制造业、客户服务等。

# 

# 在排队论中，有几个关键的术语和概念：

# 

# - 顾客到达过程：描述顾客到达系统的模式和规律，通常使用到达率（arrival rate）来表示单位时间内到达系统的顾客数量。

# - 服务过程：描述服务顾客的模式和规律，通常使用服务率（service rate）来表示单位时间内被服务的顾客数量。

# - 排队规则：描述顾客在系统中的排队方式，如先到先服务（FCFS）、后到先服务（LCFS）等。

# - 服务台数量：描述系统中服务台的数量，可以是单个或多个。

# 

# 下面，我们以M/M/1排队模型为例，给出详细的解释和Python代码示例。M/M/1模型是一种最简单的排队论模型，其中M表示顾客的到达间隔和服务时间均服从指数分布，1表示系统中只有一个服务台。

# 

# **模型假设**：

# 

# 1. 顾客到达是随机的，且相互独立，服从参数为λ的指数分布。

# 2. 服务时间也是随机的，且相互独立，服从参数为μ的指数分布。

# 3. 系统只有一个服务台，且服务规则为先到先服务（FCFS）。

# 4. 系统容量无限大，即没有排队空间限制。

# 

# **Python代码示例**：

# 

# 

import numpy as np



def mm1_queue(lambda_val, mu_val, simulation_time):

    """

    M/M/1排队模型模拟

    :param lambda_val: 顾客到达率（λ）

    :param mu_val: 服务率（μ）

    :param simulation_time: 模拟时间

    :return: 平均队长（L）、平均等待时间（W）

    """

    # 初始化变量

    time = 0  # 当前时间

    queue = []  # 队列，存储等待的顾客

    customers_served = 0  # 被服务的顾客数量

    total_waiting_time = 0  # 总等待时间



    while time < simulation_time:

        # 顾客到达

        if np.random.exponential(scale=1/lambda_val) < simulation_time - time:

            queue.append(time)  # 记录到达时间



        # 服务顾客

        if queue and np.random.exponential(scale=1/mu_val) < simulation_time - time:

            service_start_time = queue.pop(0)  # 取出队列中最早的顾客进行服务

            customers_served += 1

            waiting_time = time - service_start_time  # 计算等待时间

            total_waiting_time += waiting_time



        time += 0.01  # 时间步进



    # 计算平均队长和平均等待时间

    L = np.mean([len(queue) for t in np.arange(0, simulation_time, 0.01)])

    W = total_waiting_time / customers_served if customers_served > 0 else 0



    return L, W



# 示例参数

lambda_val = 5  # 顾客到达率（λ=5）

mu_val = 6  # 服务率（μ=6）

simulation_time = 100  # 模拟时间（100单位时间）



# 运行模拟并输出结果

L, W = mm1_queue(lambda_val, mu_val, simulation_time)

print(f"Average number of customers in the system: {L:.2f}")

print(f"Average waiting time in the system: {W:.2f}")
