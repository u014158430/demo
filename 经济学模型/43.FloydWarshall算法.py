# 

# 

# Floyd-Warshall算法是一种经典的动态规划算法，用于找出加权图中所有顶点对之间的最短路径。该算法可以处理带有负权边的图，但不能处理带有负权环的图，因为负权环会导致最短路径不收敛。下面我将对Floyd-Warshall算法进行详细解释，并提供一个Python代码示例。

# 

# ### 算法解释

# 

# Floyd-Warshall算法的基本思想是通过逐步尝试所有顶点作为中间点来更新任意两点之间的最短路径。算法使用一个二维数组（或称为距离矩阵）来存储任意两点之间的最短距离。初始时，如果两点之间存在直接边，则距离矩阵中对应位置的值就是边的权重；如果不存在直接边，则初始化为无穷大（表示不可达）。

# 

# 算法的主要步骤如下：

# 

# 1. **初始化**：设置距离矩阵dist，如果节点i和节点j之间有直接边，则dist[i][j]为边的权重；否则，dist[i][j]为无穷大。同时，将每个节点到自身的距离设为0。

# 2. **迭代更新**：对于图中的每一个节点k（作为中间点），遍历所有节点对(i, j)，如果通过节点k可以从i到达j并且路径更短（即dist[i][k] + dist[k][j] < dist[i][j]），则更新dist[i][j]的值为dist[i][k] + dist[k][j]。

# 3. **重复迭代**：重复步骤2，直到所有节点都作为中间点被考虑过。

# 

# 经过上述步骤后，距离矩阵dist中存储的就是任意两点之间的最短距离。

# 

# ### Python代码示例

# 

# 

import numpy as np

import sys



def floyd_warshall(graph):

    # 获取节点数

    n = len(graph)

    

    # 初始化距离矩阵，如果节点间没有边，则距离设为无穷大

    dist = np.full((n, n), sys.maxsize)

    for i in range(n):

        for j in range(n):

            if graph[i][j] != 0:  # 如果节点间存在边，则距离设为边的权重

                dist[i][j] = graph[i][j]

            elif i == j:  # 节点到自身的距离为0

                dist[i][j] = 0

    

    # 迭代更新距离矩阵

    for k in range(n):

        for i in range(n):

            for j in range(n):

                if dist[i][k] + dist[k][j] < dist[i][j]:

                    dist[i][j] = dist[i][k] + dist[k][j]

    

    return dist



# 示例图（邻接矩阵表示，0表示无边，其他值表示边的权重）

graph = [

    [0, 5, float('inf'), 10],

    [float('inf'), 0, 3, float('inf')],

    [float('inf'), float('inf'), 0, 1],

    [float('inf'), float('inf'), float('inf'), 0]

]



# 调用Floyd-Warshall算法

result = floyd_warshall(graph)

print("任意两点间的最短距离矩阵:")

print(result)
