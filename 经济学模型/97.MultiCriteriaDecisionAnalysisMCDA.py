# 

# 

# **Multi-Criteria Decision Analysis (MCDA) 详细解释与Python代码示例**

# 

# **一、MCDA详细解释**

# 

# Multi-Criteria Decision Analysis（MCDA），即多准则决策分析，是一种决策支持技术，旨在帮助决策者面对多个相互冲突或相互影响的准则时，做出最优的决策。在复杂的决策环境中，单一准则往往无法全面反映问题的本质，而MCDA通过整合多个准则，为决策者提供了一个系统性的分析和比较框架。

# 

# MCDA的核心思想在于将决策问题分解为多个相互关联的准则，并为每个准则分配相应的权重，以反映其在决策中的重要性。然后，通过收集与每个准则相关的数据，运用适当的评估方法，对不同的方案进行评估和比较。最后，基于评估结果，决策者可以综合考虑各个准则的权衡关系，选择出最优的方案。

# 

# MCDA在多个领域都有广泛的应用，如项目管理、环境规划、医药卫生等。在项目管理中，MCDA可以帮助项目团队在面对多个评估标准和选择方案时做出决策，确保项目的顺利实施和目标的达成。

# 

# **二、Python代码示例**

# 

# 以下是一个使用Python实现MCDA的简单示例，以层次分析法（Analytic Hierarchy Process, AHP）为基础，对不同的方案进行评估和比较。

# 

# 

import numpy as np



# 假设有三个方案（A, B, C）和三个准则（C1, C2, C3）

# 准则之间的比较矩阵（专家打分）

criteria_comparison_matrix = np.array([

    [1, 2, 3],

    [1/2, 1, 2],

    [1/3, 1/2, 1]

])



# 方案A在三个准则下的评分

scores_A = np.array([5, 4, 3])

# 方案B在三个准则下的评分

scores_B = np.array([4, 5, 2])

# 方案C在三个准则下的评分

scores_C = np.array([3, 3, 4])



# 计算准则的权重（使用numpy的linalg.eig函数求解特征值和特征向量）

eigenvalues, eigenvectors = np.linalg.eig(criteria_comparison_matrix)

max_eigenvalue_index = np.argmax(eigenvalues)

criteria_weights = eigenvectors[:, max_eigenvalue_index].real / np.sum(eigenvectors[:, max_eigenvalue_index].real)



# 计算每个方案的综合得分

score_A = np.dot(scores_A, criteria_weights)

score_B = np.dot(scores_B, criteria_weights)

score_C = np.dot(scores_C, criteria_weights)



# 输出结果

print("准则权重:", criteria_weights)

print("方案A综合得分:", score_A)

print("方案B综合得分:", score_B)

print("方案C综合得分:", score_C)



# 根据综合得分进行排序，选择最优方案

best_scheme = 'A' if score_A >= score_B and score_A >= score_C else \

             'B' if score_B >= score_A and score_B >= score_C else 'C'

print("最优方案:", best_scheme)
