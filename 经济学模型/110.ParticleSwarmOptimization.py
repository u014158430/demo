# 

# 

# 粒子群优化（Particle Swarm Optimization，简称PSO）是一种基于群体智能的优化算法，它模拟了鸟群、鱼群等生物群体的社会行为，通过个体间的信息共享和协作来寻找问题的最优解。在PSO中，每个潜在的解都被视为搜索空间中的一个“粒子”，这些粒子根据自身的历史最优位置和整个群体的历史最优位置来调整自己的速度和位置，从而逐步逼近最优解。

# 

# ### 粒子群优化算法解释

# 

# 粒子群优化算法的核心思想是通过粒子间的信息共享和协作来寻找最优解。每个粒子都代表搜索空间中的一个潜在解，并有一个位置向量和一个速度向量，分别表示其在搜索空间中的当前位置和移动速度。算法通过迭代更新粒子的位置和速度，使得粒子能够向最优解的方向移动。

# 

# 在每次迭代中，粒子会根据自己的历史最优位置（pbest）和整个群体的历史最优位置（gbest）来调整自己的速度和位置。具体来说，粒子的速度更新公式如下：

# 

# \[ v_{i}(t+1) = w \cdot v_{i}(t) + c_1 \cdot rand() \cdot (pbest_{i} - present_{i}(t)) + c_2 \cdot rand() \cdot (gbest - present_{i}(t)) \]

# 

# 其中，\(v_{i}(t+1)\) 表示粒子 \(i\) 在下一时刻的速度，\(w\) 是惯性权重，用于控制粒子当前速度对下一时刻速度的影响程度；\(c_1\) 和 \(c_2\) 是学习因子，用于调节粒子向自身历史最优位置和群体历史最优位置学习的权重；\(rand()\) 是随机数函数，用于增加算法的随机性。

# 

# 粒子的位置更新公式如下：

# 

# \[ present_{i}(t+1) = present_{i}(t) + v_{i}(t+1) \]

# 

# ### Python代码示例

# 

# 下面是一个使用Python实现的粒子群优化算法的简单示例，用于求解函数 \(y = x_1^2 + x_2^2\) 的最小值：

# 

# 

import numpy as np



# 粒子群优化算法参数

num_particles = 30  # 粒子数量

num_iterations = 100  # 迭代次数

dim = 2  # 问题维度

w = 0.8  # 惯性权重

c1 = 2  # 个体学习因子

c2 = 2  # 社会学习因子



# 初始化粒子群

particles = np.random.rand(num_particles, dim) * 10 - 5  # 初始位置在[-5, 5]之间

velocities = np.random.rand(num_particles, dim)  # 初始速度

pbest = particles.copy()  # 个体最优位置

gbest = particles[0]  # 全局最优位置

gbest_score = np.inf  # 全局最优得分



# 适应度函数

def fitness(x):

    return np.sum(x**2)



# 粒子群优化算法主循环

for t in range(num_iterations):

    for i in range(num_particles):

        # 计算当前粒子的适应度

        score = fitness(particles[i])

        

        # 更新个体最优位置

        if score < fitness(pbest[i]):

            pbest[i] = particles[i].copy()

        

        # 更新全局最优位置

        if score < gbest_score:

            gbest = particles[i].copy()

            gbest_score = score

        

        # 更新粒子速度和位置

        velocities[i] = w * velocities[i] + c1 * np.random.rand(dim) * (pbest[i] - particles[i]) + c2 * np.random.rand(dim) * (gbest - particles[i])

        particles[i] += velocities[i]

        

        # 速度限制（可选）

        velocities[i] = np.clip(velocities[i], -1, 1)  # 假设速度限制在[-1, 1]之间



# 输出结果

print("全局最优位置:", gbest)

print("全局最优得分:", gbest_score)
