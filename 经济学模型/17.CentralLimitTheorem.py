# 

# 

# **中心极限定理的详细解释与Python代码示例**

# 

# **一、中心极限定理的详细解释**

# 

# 中心极限定理是概率论和数理统计学中的一个重要定理，它揭示了大量独立同分布的随机变量之和的极限分布为正态分布的现象。简单来说，当我们从某个总体中随机抽取大量的样本，并计算这些样本的平均值时，这些平均值的分布会趋向于正态分布，无论原始总体的分布形态如何。

# 

# 中心极限定理的重要性在于，它提供了一种通过样本均值来推断总体特性的方法。在实际应用中，由于总体数据往往难以全部获取，我们通常会通过抽取样本并计算其均值来估计总体的均值。而中心极限定理告诉我们，当样本量足够大时，这种估计方法是可靠的。

# 

# **二、Python代码示例**

# 

# 下面是一个使用Python模拟中心极限定理的示例代码。在这个示例中，我们将从一个非正态分布的总体中抽取大量样本，并计算这些样本的均值，然后观察这些均值的分布是否趋向于正态分布。

# 

# 

# 导入必要的库

import numpy as np

import matplotlib.pyplot as plt



# 设置随机数种子，以便结果可复现

np.random.seed(42)



# 定义一个非正态分布的总体，这里我们使用指数分布作为示例

population = np.random.exponential(scale=1, size=100000)



# 设置样本大小和抽样次数

sample_size = 30

num_samples = 1000



# 初始化一个列表，用于存储每次抽样的均值

sample_means = []



# 循环进行抽样和计算均值

for _ in range(num_samples):

    # 从总体中随机抽取一个样本

    sample = np.random.choice(population, size=sample_size, replace=True)

    # 计算样本的均值

    mean = np.mean(sample)

    # 将均值添加到列表中

    sample_means.append(mean)



# 绘制样本均值的直方图

plt.hist(sample_means, bins=30, density=True, alpha=0.6, color='g')



# 绘制正态分布曲线作为参考（均值和标准差根据样本均值和样本标准差计算）

mean_of_means = np.mean(sample_means)

std_of_means = np.std(sample_means, ddof=1)  # ddof=1 是为了计算样本标准差

xmin, xmax = plt.xlim()

x = np.linspace(xmin, xmax, 100)

p = np.exp(-(x - mean_of_means)**2 / (2 * std_of_means**2)) / (std_of_means * np.sqrt(2 * np.pi))

plt.plot(x, p, 'k', linewidth=2)



# 设置图表标题和坐标轴标签

plt.title('Central Limit Theorem Demonstration')

plt.xlabel('Sample Mean')

plt.ylabel('Frequency')



# 显示图表

plt.show()
