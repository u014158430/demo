# 

# 

# **Waiting Line模型解释与Python代码示例**

# 

# **一、模型解释**

# 

# Waiting Line模型，也称为排队模型，是运营管理中的一个重要概念，用于描述和分析服务系统中顾客到达、排队等待、接受服务以及离开系统的过程。在现实生活中，排队现象无处不在，如银行办理业务、医院挂号、超市结账等。通过排队模型，我们可以更好地理解这些现象，并找到优化服务质量和降低成本之间的平衡点。

# 

# 排队模型通常包含以下几个要素：

# 

# 1. **顾客到达**：顾客按照一定的到达率（如平均每分钟到达人数）进入系统。

# 2. **排队规则**：顾客按照先到先服务（FCFS）、后到先服务（LCFS）或其他规则排队等待。

# 3. **服务过程**：服务台（或服务员）按照一定的服务率（如平均每分钟服务人数）为顾客提供服务。

# 4. **系统容量**：系统能够容纳的最大顾客数，超过这个数量后，新到达的顾客可能无法进入系统。

# 

# **二、Python代码示例**

# 

# 下面是一个简单的Python代码示例，用于模拟一个基本的排队模型。在这个示例中，我们假设顾客按照先到先服务的规则排队，服务台按照固定的服务率为顾客提供服务。

# 

# 

import random

import time



# 设定参数

arrival_rate = 2  # 平均每分钟到达人数

service_rate = 3  # 平均每分钟服务人数

max_queue_length = 10  # 最大排队长度

simulation_time = 60  # 模拟时间（分钟）



# 初始化队列和顾客计数器

queue = []

customers_arrived = 0

customers_served = 0



# 模拟过程

start_time = time.time()

while (time.time() - start_time) < simulation_time * 60:  # 转换为秒进行比较

    # 顾客到达

    if random.random() < arrival_rate / 60:  # 每秒都有可能到达顾客，概率由到达率决定

        customers_arrived += 1

        if len(queue) < max_queue_length:  # 如果队列未满，则顾客进入队列

            queue.append(customers_arrived)

        else:  # 如果队列已满，则顾客离开（或采取其他措施）

            print(f"顾客{customers_arrived}离开，因为队列已满。")

    

    # 服务顾客

    if queue and random.random() < service_rate / 60:  # 如果队列中有顾客且服务台空闲，则服务顾客

        customer_to_serve = queue.pop(0)  # 取出队列中的第一个顾客进行服务

        customers_served += 1

        print(f"正在为顾客{customer_to_serve}提供服务。")

        # 模拟服务时间（这里简化为固定时间，实际中可能是随机时间）

        time.sleep(1)  # 服务时间为1秒



# 模拟结束，输出结果

print(f"模拟结束。共到达顾客{customers_arrived}人，共服务顾客{customers_served}人。")
