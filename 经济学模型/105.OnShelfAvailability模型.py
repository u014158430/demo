# 

# 

# **On-Shelf Availability模型解释与Python代码示例**

# 

# **一、模型解释**

# 

# On-Shelf Availability（OSA），即货架有货率，是衡量商品在销售点（如超市货架、电商仓库等）实际可售数量与应售数量之间比例的重要指标。OSA的高低直接影响到顾客的购物体验和商家的销售业绩。一个高的OSA意味着顾客在需要时能够找到并购买到所需的商品，从而提高了顾客满意度和忠诚度。

# 

# OSA的计算公式通常为：OSA = 实际在线可售SKU数 / 应在线可售SKU数。其中，SKU（Stock Keeping Unit）是库存量单位，表示一种商品的唯一标识。

# 

# 在实际应用中，OSA的提升需要整个供应链的协同努力，包括商品采购、库存管理、物流配送、门店管理等各个环节。例如，通过精准预测销售需求，合理安排库存和补货计划，可以确保商品在销售点有足够的库存；通过优化物流配送，确保商品能够及时、准确地送达销售点；通过门店管理，确保商品在货架上摆放整齐、易于查找。

# 

# **二、Python代码示例**

# 

# 以下是一个简单的Python代码示例，用于计算OSA并输出结果：

# 

# 

# 定义一个字典，表示不同SKU的库存情况

inventory = {

    'SKU1': 100,  # SKU1的实际库存数量

    'SKU2': 80,   # SKU2的实际库存数量

    'SKU3': 0,    # SKU3缺货

    # ... 可以继续添加其他SKU的库存数量

}



# 定义一个列表，表示所有应售的SKU

available_skus = ['SKU1', 'SKU2', 'SKU3', 'SKU4']  # 假设SKU4是新增的SKU，但尚未入库



# 计算OSA

def calculate_osa(inventory, available_skus):

    """

    计算OSA

    :param inventory: 字典，表示不同SKU的库存情况

    :param available_skus: 列表，表示所有应售的SKU

    :return: OSA值，以百分比形式表示

    """

    # 计算实际在线可售SKU数

    actual_available_skus = sum(1 for sku in available_skus if sku in inventory and inventory[sku] > 0)

    # 计算应在线可售SKU数

    total_available_skus = len(available_skus)

    # 计算OSA

    osa = (actual_available_skus / total_available_skus) * 100

    return osa



# 调用函数并输出结果

osa_value = calculate_osa(inventory, available_skus)

print(f"当前OSA值为：{osa_value:.2f}%")



# 注释：

# 1. inventory字典表示了不同SKU的库存情况，其中SKU3缺货。

# 2. available_skus列表表示了所有应售的SKU，包括尚未入库的SKU4。

# 3. calculate_osa函数通过遍历available_skus列表，检查每个SKU是否在inventory字典中且库存数量大于0，来计算实际在线可售SKU数。

# 4. 然后，函数计算应在线可售SKU数（即available_skus列表的长度），并据此计算OSA值。

# 5. 最后，函数返回OSA值，并以百分比形式表示。
