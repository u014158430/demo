# 

# 

# 泰尔指数（Theil Index）是一种广泛应用于经济学、社会学和政策研究中的统计指标，用于衡量收入或财富分配的不平等程度。该指数由经济学家Henri Theil在20世纪60年代提出，基于信息理论中的熵概念，通过计算个体或群体间收入份额的差异来反映收入分配的平等性。

# 

# ### 泰尔指数的解释

# 

# 泰尔指数的值域通常在0到1之间，其中0代表完全平等，即所有个体或群体拥有相同的收入份额；而1则代表最大的不平等，即所有收入或财富集中在一个个体或群体中。泰尔指数越高，说明收入或财富分配的不平等程度越大。

# 

# 泰尔指数的计算通常涉及将个体或群体的收入按照从低到高的顺序排列，然后计算每个收入区间相对于总收入的比例，并基于这些比例计算出一个反映不平等程度的指数。泰尔指数的一个关键特性是它可以同时衡量组内差距和组间差距对总差距的贡献，这使得它在分析复杂的社会经济结构时特别有用。

# 

# ### Python代码示例

# 

# 下面是一个使用Python计算泰尔指数的示例代码。假设我们有一个包含个体收入数据的Pandas DataFrame，名为`income_data`，其中`income`列代表个体的收入。

# 

# 

import pandas as pd

import numpy as np



# 假设income_data是一个包含个体收入数据的DataFrame

# income_data = pd.read_csv('income_data.csv')  # 这里省略了读取数据的步骤



# 示例数据

income_data = pd.DataFrame({

    'income': [1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000]

})



# 将收入数据按升序排序

income_data_sorted = income_data.sort_values(by='income')



# 计算每个个体的收入份额

income_shares = income_data_sorted['income'] / income_data_sorted['income'].sum()



# 初始化泰尔指数为0

theil_index = 0



# 计算泰尔指数

for i in range(len(income_shares)):

    # 计算每个收入份额对泰尔指数的贡献

    theil_index += income_shares.iloc[i] * np.log(income_shares.iloc[i])



# 泰尔指数是log(n)减去上述计算的结果，其中n是收入数据的数量

n = len(income_shares)

theil_index = np.log(n) - theil_index



# 输出泰尔指数

print(f"泰尔指数为: {theil_index:.4f}")



# 注释：

# 1. 首先，我们导入了pandas和numpy库，用于数据处理和数学计算。

# 2. 然后，我们假设有一个包含个体收入数据的DataFrame（这里为了示例，我们直接创建了一个）。

# 3. 接着，我们将收入数据按升序排序，并计算每个个体的收入份额。

# 4. 我们初始化泰尔指数为0，并遍历每个收入份额，计算其对泰尔指数的贡献。

# 5. 最后，我们通过log(n)减去上述计算的结果得到最终的泰尔指数，并输出。
