# 

# 

# 高斯混合模型（Gaussian Mixture Model，简称GMM）是一种强大的统计工具，用于描述和模拟复杂的数据分布。它假设数据是由多个高斯分布（也称为正态分布）混合而成的，每个高斯分布代表数据中的一个潜在类别或簇。通过估计这些高斯分布的参数（如均值、协方差矩阵和混合权重），GMM可以对数据集进行聚类，并生成新的数据样本。

# 

# ### 解释

# 

# GMM的核心思想是将数据集中的每个数据点视为来自K个高斯分布中某一个的样本，但事先并不知道每个数据点具体来自哪个分布。因此，GMM试图通过最大化数据的似然函数来估计这些高斯分布的参数。这个过程通常使用期望最大化（Expectation-Maximization，EM）算法来实现。

# 

# 在GMM中，每个高斯分布都由其均值向量、协方差矩阵和混合权重来描述。均值向量决定了分布的中心位置，协方差矩阵决定了分布的形状和方向，而混合权重则决定了每个分布在混合模型中的相对重要性。

# 

# ### Python代码示例

# 

# 下面是一个使用Python和`scikit-learn`库实现GMM的示例代码：

# 

# 

# 导入必要的库

from sklearn.mixture import GaussianMixture

import numpy as np

import matplotlib.pyplot as plt

from scipy.stats import multivariate_normal



# 生成模拟数据

np.random.seed(0)

n_samples = 300

means = [(0, 0), (5, 4), (1, 2)]

covariances = [[1, 0], [1, 1], [0.5, 0.5]]

X = np.vstack([multivariate_normal(mean, cov).rvs(n_samples)

               for mean, cov in zip(means, covariances)])



# 初始化GMM模型并拟合数据

gmm = GaussianMixture(n_components=3)  # 假设有3个高斯分布

gmm.fit(X)



# 预测数据点的聚类标签和概率

labels = gmm.predict(X)

probs = gmm.predict_proba(X)



# 绘制结果

plt.scatter(X[:, 0], X[:, 1], c=labels, s=40, cmap='viridis')



# 绘制每个高斯分布的等高线图

for i, (weight, mean, cov) in enumerate(zip(gmm.weights_, gmm.means_, gmm.covariances_)):

    v, w = np.linalg.eigh(cov)

    u = w[0] / np.linalg.norm(w[0])

    angle = np.arctan2(u[1], u[0])

    angle = 180 * angle / np.pi  # 将弧度转换为角度

    ell = mpl.patches.Ellipse(mean, 2 * np.sqrt(v[0]), 2 * np.sqrt(v[1]),

                              180 + angle, color=plt.cm.viridis(i / len(gmm.means_)))

    ell.set_alpha(0.5)

    plt.gca().add_artist(ell)



plt.title('Gaussian Mixture Model')

plt.xlabel('Feature 1')

plt.ylabel('Feature 2')

plt.show()
